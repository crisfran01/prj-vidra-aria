-- MySQL dump 10.13  Distrib 8.0.17, for Linux (x86_64)
--
-- Host: localhost    Database: vidracaria
-- ------------------------------------------------------
-- Server version	8.0.17-0ubuntu2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agenda` (
  `Cod_registro` int(11) NOT NULL AUTO_INCREMENT,
  `CodP` int(11) NOT NULL,
  `Titulo` varchar(128) NOT NULL,
  `Descricao` text,
  `Data` date DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `Status` varchar(24) DEFAULT 'Incompleto',
  PRIMARY KEY (`Cod_registro`),
  KEY `CodP` (`CodP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caixa`
--

DROP TABLE IF EXISTS `caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `caixa` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_conta` int(11) DEFAULT NULL,
  `valor` float DEFAULT '1',
  `cod_fun` int(11) DEFAULT NULL,
  `data` varchar(20) DEFAULT NULL,
  `obs` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caixa`
--

LOCK TABLES `caixa` WRITE;
/*!40000 ALTER TABLE `caixa` DISABLE KEYS */;
/*!40000 ALTER TABLE `caixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clientes` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `apelido` varchar(20) DEFAULT NULL,
  `nascimento` varchar(30) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `CPF` varchar(14) DEFAULT NULL,
  `RG` varchar(12) DEFAULT NULL,
  `end` varchar(50) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `bairro` varchar(15) DEFAULT NULL,
  `cidade` varchar(20) DEFAULT NULL,
  `UF` varchar(2) NOT NULL,
  `CEP` varchar(9) NOT NULL,
  `tel` varchar(13) DEFAULT NULL,
  `whats` varchar(30) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `obs` varchar(300) DEFAULT NULL,
  `person` varchar(30) DEFAULT NULL,
  `cod_fun` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Cristiane Franchini','Cris',NULL,'F','31544632835','49510257x','Av. 6 de agosto 212',NULL,'Vila nova','Pirassununga','','','1930552353','19996086192','crisfr01@gmail.com',NULL,NULL,NULL),(2,'Cristiane Franchini',NULL,NULL,NULL,'31544632825','49510257x','Av. 6 de agosto 212',NULL,'Vila nova ','Pirassununga','','','1930552353','19996086192','crisfr01@gmail.com',NULL,NULL,NULL),(3,'Transportadora Amente',NULL,NULL,NULL,'30','0','R. Pereira Bueno 1137',NULL,'Rosario','Pirassununga','SP','13634310','3561-5387','97160878',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condpg`
--

DROP TABLE IF EXISTS `condpg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `condpg` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(200) DEFAULT NULL,
  `parcelas` int(11) DEFAULT '1',
  `data_first` int(11) DEFAULT '0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condpg`
--

LOCK TABLES `condpg` WRITE;
/*!40000 ALTER TABLE `condpg` DISABLE KEYS */;
INSERT INTO `condpg` VALUES (1,'10 Dias',1,10),(2,'A vista',1,0),(3,'3 x',3,0),(4,'3 x sem entrada',3,30);
/*!40000 ALTER TABLE `condpg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contas_receber`
--

DROP TABLE IF EXISTS `contas_receber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contas_receber` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_vend` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `data_criacao` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contas_receber`
--

LOCK TABLES `contas_receber` WRITE;
/*!40000 ALTER TABLE `contas_receber` DISABLE KEYS */;
/*!40000 ALTER TABLE `contas_receber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fornecedores` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome_fantasia` varchar(150) DEFAULT NULL,
  `razao_social` varchar(20) DEFAULT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `ie` varchar(15) DEFAULT NULL,
  `end` varchar(30) DEFAULT NULL,
  `UF` varchar(2) DEFAULT NULL,
  `CEP` varchar(9) DEFAULT NULL,
  `bairro` varchar(15) DEFAULT NULL,
  `Cidade` varchar(15) DEFAULT NULL,
  `tel` varchar(13) DEFAULT NULL,
  `whats` varchar(30) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `servico` varchar(20) DEFAULT NULL,
  `obs` varchar(300) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `cod_fun` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores`
--

LOCK TABLES `fornecedores` WRITE;
/*!40000 ALTER TABLE `fornecedores` DISABLE KEYS */;
INSERT INTO `fornecedores` VALUES (1,NULL,'AlGlas','35273126389380','','R. 13 de maio 2230','SP','136863293','Vl industrial','São Paulo','1376136892173','1378947896589','Al.glas@123.com','Aluminio',NULL,NULL,NULL),(2,NULL,'StarTemper','46872648917203','','rua oliveiras 422','SP','739472984','Santa rita','Campinas','7489372894720','3092748972389','Startemper@gmail.com','Vidro temperado',NULL,NULL,NULL);
/*!40000 ALTER TABLE `fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `full_item`
--

DROP TABLE IF EXISTS `full_item`;
/*!50001 DROP VIEW IF EXISTS `full_item`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `full_item` AS SELECT 
 1 AS `cod`,
 1 AS `nome`,
 1 AS `cod_vend`,
 1 AS `cod_prod`,
 1 AS `altura`,
 1 AS `largura`,
 1 AS `metragem`,
 1 AS `qtde`,
 1 AS `preco`,
 1 AS `pr_venda`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `full_sale`
--

DROP TABLE IF EXISTS `full_sale`;
/*!50001 DROP VIEW IF EXISTS `full_sale`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `full_sale` AS SELECT 
 1 AS `cod`,
 1 AS `obs`,
 1 AS `data`,
 1 AS `nome`,
 1 AS `cod_fun`,
 1 AS `cod_cli`,
 1 AS `cod_pg`,
 1 AS `valor`,
 1 AS `name_pg`,
 1 AS `name_fun`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `full_user`
--

DROP TABLE IF EXISTS `full_user`;
/*!50001 DROP VIEW IF EXISTS `full_user`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `full_user` AS SELECT 
 1 AS `cod`,
 1 AS `username`,
 1 AS `nome`,
 1 AS `senha`,
 1 AS `nivel`,
 1 AS `cod_fun`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `funcionarios`
--

DROP TABLE IF EXISTS `funcionarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `funcionarios` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `cpf` varchar(12) NOT NULL,
  `rg` varchar(12) DEFAULT NULL,
  `end` varchar(200) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `bairro` varchar(40) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `cel` varchar(12) DEFAULT NULL,
  `email` int(11) DEFAULT NULL,
  `funcao` varchar(20) DEFAULT NULL,
  `FGTS` varchar(20) DEFAULT NULL,
  `nascimento` varchar(30) DEFAULT NULL,
  `obs` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`cod`),
  UNIQUE KEY `cpf` (`cpf`,`rg`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcionarios`
--

LOCK TABLES `funcionarios` WRITE;
/*!40000 ALTER TABLE `funcionarios` DISABLE KEYS */;
INSERT INTO `funcionarios` VALUES (1,'teste','12131313131','',NULL,NULL,NULL,NULL,NULL,'','','',NULL,NULL,'',NULL,NULL);
/*!40000 ALTER TABLE `funcionarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_vend` int(11) DEFAULT NULL,
  `cod_prod` int(11) DEFAULT NULL,
  `altura` float DEFAULT NULL,
  `largura` float DEFAULT NULL,
  `metragem` float DEFAULT NULL,
  `qtde` float DEFAULT NULL,
  `preco` float DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itens`
--

DROP TABLE IF EXISTS `itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `itens` (
  `cod_vend` int(11) DEFAULT NULL,
  `cod_prod` int(11) DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `largura` int(11) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `metragem` int(11) DEFAULT NULL,
  `Preco` int(11) DEFAULT NULL,
  KEY `codVend_fk` (`cod_vend`),
  KEY `codProd_fk` (`cod_prod`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itens`
--

LOCK TABLES `itens` WRITE;
/*!40000 ALTER TABLE `itens` DISABLE KEYS */;
INSERT INTO `itens` VALUES (1,1,1,1,1,1,100),(2,3,1,1,1,1,20),(2,2,2,2,2,8,80),(3,1,1,1,1,1,100);
/*!40000 ALTER TABLE `itens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parcelas`
--

DROP TABLE IF EXISTS `parcelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parcelas` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_conta` int(11) DEFAULT NULL,
  `vencimento` varchar(11) DEFAULT '1',
  `valor` float DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parcelas`
--

LOCK TABLES `parcelas` WRITE;
/*!40000 ALTER TABLE `parcelas` DISABLE KEYS */;
/*!40000 ALTER TABLE `parcelas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `produtos` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(30) DEFAULT NULL,
  `pr_custo` int(11) DEFAULT NULL,
  `pr_venda` int(11) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `unid_med` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` VALUES (1,'','Vidro Pontilhado',100,200,100,'m2'),(2,'','Vidro Cane',10,20,200,'m2'),(3,'','Tucano grande',20,30,10,'un');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `senha` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nivel` char(1) DEFAULT NULL,
  `cod_fun` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'admin','123','1',NULL),(2,'Cristiane','1992','1',NULL),(3,'cris','1992','1',NULL),(4,'Fabio','Fabio','2',NULL),(5,'Fatima','j0198556','1',NULL),(10,'cris','123','1',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendas`
--

DROP TABLE IF EXISTS `vendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendas` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_cli` int(11) DEFAULT NULL,
  `data` varchar(30) DEFAULT NULL,
  `Obs` varchar(300) DEFAULT NULL,
  `cod_fun` int(11) DEFAULT NULL,
  `cod_pg` int(11) DEFAULT NULL,
  `Valor` int(11) NOT NULL,
  PRIMARY KEY (`cod`),
  KEY `codPG_fk` (`cod_pg`),
  KEY `codFun_fk` (`cod_fun`),
  KEY `codCli_fk` (`cod_cli`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendas`
--

LOCK TABLES `vendas` WRITE;
/*!40000 ALTER TABLE `vendas` DISABLE KEYS */;
INSERT INTO `vendas` VALUES (1,2,NULL,'1',1,1,100),(2,3,NULL,NULL,NULL,NULL,100),(3,1,'2018-05-13','1',1,1,100);
/*!40000 ALTER TABLE `vendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `full_item`
--

/*!50001 DROP VIEW IF EXISTS `full_item`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`user`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `full_item` AS select `i`.`cod` AS `cod`,`p`.`nome` AS `nome`,`i`.`cod_vend` AS `cod_vend`,`i`.`cod_prod` AS `cod_prod`,`i`.`altura` AS `altura`,`i`.`largura` AS `largura`,`i`.`metragem` AS `metragem`,`i`.`qtde` AS `qtde`,`i`.`preco` AS `preco`,`p`.`pr_venda` AS `pr_venda` from (`items` `i` join `produtos` `p` on((`p`.`cod` = `i`.`cod_prod`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `full_sale`
--

/*!50001 DROP VIEW IF EXISTS `full_sale`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`user`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `full_sale` AS select `v`.`cod` AS `cod`,`v`.`Obs` AS `obs`,`v`.`data` AS `data`,`c`.`nome` AS `nome`,`v`.`cod_fun` AS `cod_fun`,`v`.`cod_cli` AS `cod_cli`,`v`.`cod_pg` AS `cod_pg`,`v`.`Valor` AS `valor`,`p`.`descricao` AS `name_pg`,`f`.`nome` AS `name_fun` from (((`vendas` `v` join `condpg` `p` on((`p`.`cod` = `v`.`cod_pg`))) join `funcionarios` `f` on((`p`.`cod` = `v`.`cod_fun`))) join `clientes` `c` on((`p`.`cod` = `v`.`cod_cli`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `full_user`
--

/*!50001 DROP VIEW IF EXISTS `full_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`user`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `full_user` AS select `u`.`cod` AS `cod`,`u`.`nome` AS `username`,`f`.`nome` AS `nome`,`u`.`senha` AS `senha`,`u`.`nivel` AS `nivel`,`u`.`cod_fun` AS `cod_fun` from (`usuarios` `u` join `funcionarios` `f` on((`f`.`cod` = `u`.`cod_fun`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-29 21:45:41
