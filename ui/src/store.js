import { applyMiddleware, createStore, combineReducers } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"
import clientsReducer from "./reducers/ClientsReducer"
import employReducer from "./reducers/EmployReducer"
import productReducer from "./reducers/ProductReducer"
import sellersReducer from "./reducers/SellersReducer"
import saleReducer from "./reducers/SaleReducer"
import itemReducer from "./reducers/ItemReducer"
import userReducer from "./reducers/UserReducer"
import condPgReducer from "./reducers/CondPgReducer"

const reducer = combineReducers({
    clientsReducer,
    employReducer,
    productReducer,
    sellersReducer,
    saleReducer,
    itemReducer,
    userReducer,
    condPgReducer
})

const middleware = applyMiddleware(thunk)
const enhancers = composeWithDevTools(middleware)

export default createStore(reducer, enhancers)
