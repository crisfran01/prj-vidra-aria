import axios from "axios"

export const fetchSale = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/sales`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            dispatch(fetchSaleSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchSaleError(err))
        })
}

export const fetchSaleSuccess = sale => ({
    type: "FETCH_SALES_SUCCESS",
    payload: sale
})

export const fetchSaleError = err => ({
    type: "FETCH_SALES_ERROR",
    payload: err
})


export const createSale = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/sale`, data)
        .then(response => {
            dispatch(createSaleSuccess(response.data['data']))
        })
        .catch(err => {
           
            dispatch(createSaleError(err))
        })
}

export const createSaleSuccess = sale => ({
    type: "CREATE_SALES_SUCCESS",
    payload: sale
})

export const createSaleError = err => ({
    type: "CREATE_SALES_ERROR",
    payload: err
})


export const updateSale = (data, code) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/sale/` + code, data)
        .then(response => {
            dispatch(updateSaleSuccess())
        })
        .catch(err => {
           
            dispatch(updateSaleError(err))
        })
}

export const updateSaleSuccess = () => ({
    type: "UPDATE_SALE_SUCCESS"
})

export const updateSaleError = err => ({
    type: "UPDATE_SALE_ERROR",
    payload: err
})


export const getSaleNumb = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/sale/new-code`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            dispatch(getSaleNumbSuccess(response.data))
        })
        .catch(err => {
            dispatch(getSaleNumbError(err))
        })
}

export const getSaleNumbSuccess = sale => ({
    type: "GET_SALE_NUMB_SUCCESS",
    payload: sale
})

export const getSaleNumbError = err => ({
    type: "GET_SALE_NUMB_ERROR",
    payload: err
})

