import axios from "axios"

export const fetchEmploys = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/employs`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
        .then(response => {
            dispatch(fetchEmploysSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchEmploysError(err))
        })
}

export const fetchEmploysSuccess = employs => ({
    type: "FETCH_EMPLOYS_SUCCESS",
    payload: employs
})

export const fetchEmploysError = err => ({
    type: "FETCH_EMPLOYS_ERROR",
    payload: err
})


export const createEmploy = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/employ`, data)
        .then(response => {
            dispatch(createEmploysSuccess())
        })
        .catch(err => {
           
            dispatch(createEmploysError(err))
        })
}

export const createEmploysSuccess = message => ({
    type: "CREATE_EMPLOYS_SUCCESS"
})

export const createEmploysError = err => ({
    type: "CREATE_EMPLOYS_ERROR",
    payload: err
})


export const updateEmploy = (data, code) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/employ/` + code, data)
        .then(response => {
            dispatch(updateEmploysSuccess())
        })
        .catch(err => {
           
            dispatch(updateEmploysError(err))
        })
}

export const updateEmploysSuccess = () => ({
    type: "UPDATE_EMPLOY_SUCCESS"
})

export const updateEmploysError = err => ({
    type: "UPDATE_EMPLOY_ERROR",
    payload: err
})

