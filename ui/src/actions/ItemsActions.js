import axios from "axios"

export const fetchItem = (code) => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/sale/` + code + `/items`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            console.log(response)
            dispatch(fetchItemSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchItemError(err))
        })
}

export const fetchItemSuccess = item => ({
    type: "FETCH_ITEM_SUCCESS",
    payload: item
})

export const fetchItemError = err => ({
    type: "FETCH_ITEM_ERROR",
    payload: err
})


export const createItem = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/item`, data)
        .then(response => {
            dispatch(createItemSuccess('a'))
        })
        .catch(err => {
           
            dispatch(createItemError(err))
        })
}

export const createItemSuccess = message => ({
    type: "CREATE_ITEM_SUCCESS"
})

export const createItemError = err => ({
    type: "CREATE_ITEM_ERROR",
    payload: err
})


export const updateItem = (data, code) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/items/` + code, data)
        .then(response => {
            dispatch(updateItemSuccess())
        })
        .catch(err => {
           
            dispatch(updateItemError(err))
        })
}

export const updateItemSuccess = () => ({
    type: "UPDATE_ITEM_SUCCESS"
})

export const updateItemError = err => ({
    type: "UPDATE_ITEM_ERROR",
    payload: err
})

