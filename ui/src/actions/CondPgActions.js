import axios from "axios"

export const fetchCondPgs = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/condPgs`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            dispatch(fetchcondPgsSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchcondPgsError(err))
        })
}

export const fetchcondPgsSuccess = condPgs => ({
    type: "FETCH_CONDPGS_SUCCESS",
    payload: condPgs
})

export const fetchcondPgsError = err => ({
    type: "FETCH_CONDPGS_ERROR",
    payload: err
})


export const createCondPg = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/condPg`, data)
        .then(response => {
            dispatch(createCondPgsSuccess())
            dispatch(fetchCondPgs())

        })
        .catch(err => {
           
            dispatch(createCondPgsError(err))
        })
}

export const createCondPgsSuccess = message => ({
    type: "CREATE_CONDPGS_SUCCESS"
})

export const createCondPgsError = err => ({
    type: "CREATE_CONDPGS_ERROR",
    payload: err
})


export const updateCondPg = (data, code) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/condPg/` + code, data)
        .then(response => {
            dispatch(updateCondPgsSuccess())
            dispatch(fetchCondPgs())
        })
        .catch(err => {
           
            dispatch(updateCondPgsError(err))
        })
}

export const updateCondPgsSuccess = () => ({
    type: "UPDATE_CONDPG_SUCCESS"
})

export const updateCondPgsError = err => ({
    type: "UPDATE_CONDPG_ERROR",
    payload: err
})


export const deleteCondPg = (code) => dispatch => {
    axios
        .delete(`http://127.0.0.1:8000/api/condPg/` + code)
        .then(response => {
            dispatch(deleteCondPgsSuccess())
            dispatch(fetchCondPgs())
        })
        .catch(err => {
           
            dispatch(deleteCondPgsError(err))
        })
}

export const deleteCondPgsSuccess = () => ({
    type: "DELETE_CONDPG_SUCCESS"
})

export const deleteCondPgsError = err => ({
    type: "DELETE_CONDPG_ERROR",
    payload: err
})


