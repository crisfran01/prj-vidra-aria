import axios from "axios"

export const fetchClients = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/clients`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            dispatch(fetchclientsSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchclientsError(err))
        })
}

export const fetchclientsSuccess = clients => ({
    type: "FETCH_CLIENTS_SUCCESS",
    payload: clients
})

export const fetchclientsError = err => ({
    type: "FETCH_CLIENTS_ERROR",
    payload: err
})


export const createClient = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/client`, data)
        .then(response => {
            dispatch(createClientsSuccess('a'))
        })
        .catch(err => {
           
            dispatch(createClientsError(err))
        })
}

export const createClientsSuccess = message => ({
    type: "CREATE_CLIENTS_SUCCESS"
})

export const createClientsError = err => ({
    type: "CREATE_CLIENTS_ERROR",
    payload: err
})


export const updateClient = (data, code) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/client/` + code, data)
        .then(response => {
            dispatch(updateClientsSuccess())
        })
        .catch(err => {
           
            dispatch(updateClientsError(err))
        })
}

export const updateClientsSuccess = () => ({
    type: "UPDATE_CLIENT_SUCCESS"
})

export const updateClientsError = err => ({
    type: "UPDATE_CLIENT_ERROR",
    payload: err
})

