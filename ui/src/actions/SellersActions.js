import axios from "axios"

export const fetchSellers = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/sellers`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            dispatch(fetchsellersSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchsellersError(err))
        })
}

export const fetchsellersSuccess = sellers => ({
    type: "FETCH_SELLERS_SUCCESS",
    payload: sellers
})

export const fetchsellersError = err => ({
    type: "FETCH_SELLERS_ERROR",
    payload: err
})


export const createSeller = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/seller`, data)
        .then(response => {
            dispatch(createSellersSuccess('a'))
        })
        .catch(err => {
           
            dispatch(createSellersError(err))
        })
}

export const createSellersSuccess = message => ({
    type: "CREATE_SELLERS_SUCCESS"
})

export const createSellersError = err => ({
    type: "CREATE_SELLERS_ERROR",
    payload: err
})


export const updateSeller = (data, cod) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/seller/` + cod, data, cod)
        .then(response => {
            dispatch(updateSellersSuccess())
        })
        .catch(err => {
           
            dispatch(updateSellersError(err))
        })
}

export const updateSellersSuccess = () => ({
    type: "UPDATE_SELLER_SUCCESS"
})

export const updateSellersError = err => ({
    type: "UPDATE_SELLER_ERROR",
    payload: err
})

