import axios from "axios"

export const fetchUsers = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/users`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            dispatch(fetchusersSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchusersError(err))
        })
}

export const fetchusersSuccess = users => ({
    type: "FETCH_USERS_SUCCESS",
    payload: users
})

export const fetchusersError = err => ({
    type: "FETCH_USERS_ERROR",
    payload: err
})


export const createUser = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/user`, data)
        .then(response => {
            dispatch(createUsersSuccess())
            dispatch(fetchUsers())
        })
        .catch(err => {
           
            dispatch(createUsersError(err))
        })
}

export const createUsersSuccess = message => ({
    type: "CREATE_USERS_SUCCESS"
})

export const createUsersError = err => ({
    type: "CREATE_USERS_ERROR",
    payload: err
})


export const updateUser = (data, code) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/user/` + code, data)
        .then(response => {
            dispatch(updateUsersSuccess())
            dispatch(fetchUsers())
        })
        .catch(err => {
           
            dispatch(updateUsersError(err))
        })
}

export const updateUsersSuccess = () => ({
    type: "UPDATE_USER_SUCCESS"
})

export const updateUsersError = err => ({
    type: "UPDATE_USER_ERROR",
    payload: err
})


export const deleteUser = (code) => dispatch => {
    axios
        .delete(`http://127.0.0.1:8000/api/user/` + code)
        .then(response => {
            dispatch(deleteUsersSuccess())
            dispatch(fetchUsers())
        })
        .catch(err => {
           
            dispatch(deleteUsersError(err))
        })
}

export const deleteUsersSuccess = () => ({
    type: "DELETE_USER_SUCCESS"
})

export const deleteUsersError = err => ({
    type: "DELETE_USER_ERROR",
    payload: err
})


export const checkLogin = (code) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/user/login`)
        .then(response => {
            dispatch(checkLoginSuccess())
            dispatch(fetchUsers())
        })
        .catch(err => {
           
            dispatch(checkLoginError(err))
        })
}

export const checkLoginSuccess = () => ({
    type: "CHECK_LOGIN_SUCCESS"
})

export const checkLoginError = err => ({
    type: "CHECK_LOGIN_ERROR",
    payload: err
})


