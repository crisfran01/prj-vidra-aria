import axios from "axios"

export const fetchProducts = () => dispatch => {
    axios
        .get(`http://127.0.0.1:8000/api/products`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            }
        })
        .then(response => {
            console.log(response)
            dispatch(fetchProductsSuccess(response.data))
        })
        .catch(err => {
            dispatch(fetchProductsError(err))
        })
}

export const fetchProductsSuccess = products => ({
    type: "FETCH_PRODUCTS_SUCCESS",
    payload: products
})

export const fetchProductsError = err => ({
    type: "FETCH_PRODUCTS_ERROR",
    payload: err
})


export const createProduct = (data) => dispatch => {
    axios
        .post(`http://127.0.0.1:8000/api/product`, data)
        .then(response => {
            dispatch(createProductsSuccess())
        })
        .catch(err => {
           
            dispatch(createProductsError(err))
        })
}

export const createProductsSuccess = message => ({
    type: "CREATE_PRODUCTS_SUCCESS"
})

export const createProductsError = err => ({
    type: "CREATE_PRODUCTS_ERROR",
    payload: err
})


export const updateProduct = (data, code) => dispatch => {
    axios
        .put(`http://127.0.0.1:8000/api/product/`+ code, data)
        .then(response => {
            dispatch(updateProductsSuccess())
        })
        .catch(err => {
           
            dispatch(updateProductsError(err))
        })
}

export const updateProductsSuccess = () => ({
    type: "UPDATE_PRODUCT_SUCCESS"
})

export const updateProductsError = err => ({
    type: "UPDATE_PRODUCT_ERROR",
    payload: err
})

