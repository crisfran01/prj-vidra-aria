
const initialState = {
    condPgs: []
}

const condPgsCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_CONDPGS_SUCCESS":
            return { ...state, condPgs: action.payload }
        case "FETCH_CONDPGS_ERROR":
            return { ...state, error: action.payload }
        case "CREATE_CONDPGS_SUCCESS":
            return { ...state, createCondPg: true }
        case "CREATE_CONDPGS_ERROR":
            return { ...state, createCondPg: false ,error: action.payload }   
        case "UPDATE_CONDPG_SUCCESS":
            return { ...state, updateCondPg: true }
        case "UPDATE_CONDPG_ERROR":
            return { ...state,updateCondPg: false, error: action.payload }    
        case "DELETE_CONDPG_SUCCESS":
            return { ...state, updateCondPg: true }
        case "DELETE_CONDPG_ERROR":
            return { ...state,updateCondPg: false, error: action.payload }    
        
        default:
            return state
    }
}

export default condPgsCreate
