const initialState = {
    employs: []
}

const employsCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_EMPLOYS_SUCCESS":
            return { ...state, employs: action.payload }
        case "FETCH_EMPLOYS_ERROR":
            return { ...state, error: action.payload }
        case "CREATE_EMPLOYS_SUCCESS":
            return { ...state, createEmploy: true }
        case "CREATE_EMPLOYS_ERROR":
            return { ...state, createEmploy: false ,error: action.payload }   
        case "UPDATE_EMPLOY_SUCCESS":
            return { ...state, updateEmploy: true }
        case "UPDATE_EMPLOY_ERROR":
            return { ...state,updateEmploy: false, error: action.payload }    
        default:
            return state
    }
}

export default employsCreate
