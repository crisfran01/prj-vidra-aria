const initialState = {
    clients: []
}

const clientsCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_CLIENTS_SUCCESS":
            return { ...state, clients: action.payload }
        case "FETCH_CLIENTS_ERROR":
            return { ...state, error: action.payload }
        case "CREATE_CLIENTS_SUCCESS":
            return { ...state, createClient: true }
        case "CREATE_CLIENTS_ERROR":
            return { ...state, createClient: false ,error: action.payload }   
        case "UPDATE_CLIENT_SUCCESS":
            return { ...state, updateClient: true }
        case "UPDATE_CLIENT_ERROR":
            return { ...state,updateClient: false, error: action.payload }    
        default:
            return state
    }
}

export default clientsCreate
