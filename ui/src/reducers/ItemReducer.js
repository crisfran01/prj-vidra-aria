const initialState = {
    item: []
}

const itemCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_ITEM_SUCCESS":
            return { ...state, item: action.payload }
        case "FETCH_ITEM_ERROR":
            return { ...state, error: action.payload }
        case "CREATE_ITEM_SUCCESS":
            return { ...state, createItem: true }
        case "CREATE_ITEM_ERROR":
            return { ...state, createItem: false ,error: action.payload }   
        case "UPDATE_ITEM_SUCCESS":
            return { ...state, updateItem: true }
        case "UPDATE_ITEM_ERROR":
            return { ...state,updateItem: false, error: action.payload }    
        default:
            return state
    }
}

export default itemCreate
