
const initialState = {
    users: []
}

const usersCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_USERS_SUCCESS":
            return { ...state, users: action.payload }
        case "FETCH_USERS_ERROR":
            return { ...state, error: action.payload }
        case "CHECK_LOGIN_SUCCESS":
            return { ...state, login:false, usersId: action.payload }
        case "CHECK_LOGIN_ERROR":
            return { ...state, login: false }
        case "CREATE_USERS_SUCCESS":
            return { ...state, createUser: true }
        case "CREATE_USERS_ERROR":
            return { ...state, createUser: false ,error: action.payload }   
            case "UPDATE_USER_SUCCESS":
            return { ...state, updateUser: true }
        case "UPDATE_USER_ERROR":
            return { ...state,updateUser: false, error: action.payload }    
        case "DELETE_USER_SUCCESS":
            return { ...state, updateUser: true }
        case "DELETE_USER_ERROR":
            return { ...state,updateUser: false, error: action.payload }    
        default:
            return state
    }
}

export default usersCreate
