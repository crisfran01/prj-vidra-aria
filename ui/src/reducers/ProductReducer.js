const initialState = {
    products: []
}

const productsCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_PRODUCTS_SUCCESS":
            return { ...state, products: action.payload }
        case "FETCH_PRODUCTS_ERROR":
            return { ...state, error: action.payload }
        case "CREATE_PRODUCTS_SUCCESS":
            return { ...state, createProduct: true }
        case "CREATE_PRODUCTS_ERROR":
            return { ...state, createProduct: false ,error: action.payload }   
        case "UPDATE_PRODUCT_SUCCESS":
            return { ...state, updateProduct: true }
        case "UPDATE_PRODUCT_ERROR":
            return { ...state,updateProduct: false, error: action.payload }    
        default:
            return state
    }
}

export default productsCreate
