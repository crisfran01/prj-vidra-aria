const initialState = {
    sale: []
}

const saleCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_SALES_SUCCESS":
            return { ...state, sale: action.payload }
        case "FETCH_SALES_ERROR":
            return { ...state, error: action.payload }
        case "GET_SALE_NUMB_SUCCESS":
            return { ...state, newSale: action.payload }
        case "GET_SALE_NUMB_ERROR":
            return { ...state, error: action.payload }
        case "CREATE_SALES_SUCCESS":
            return { ...state, sale: action.payload }
        case "CREATE_SALES_ERROR":
            return { ...state, createSale: false ,error: action.payload }   
        case "UPDATE_SALES_SUCCESS":
            return { ...state, updateSale: true }
        case "UPDATE_SALES_ERROR":
            return { ...state,updateSale: false, error: action.payload }    
        default:
            return state
    }
}

export default saleCreate
