const initialState = {
    sellers: []
}

const sellersCreate = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_SELLERS_SUCCESS":
            return { ...state, sellers: action.payload }
        case "FETCH_SELLERS_ERROR":
            return { ...state, error: action.payload }
        case "CREATE_SELLERS_SUCCESS":
            return { ...state, createSeller: true }
        case "CREATE_SELLERS_ERROR":
            return { ...state, createSeller: false ,error: action.payload }   
        case "UPDATE_SELLER_SUCCESS":
            return { ...state, updateSeller: true }
        case "UPDATE_SELLER_ERROR":
            return { ...state,updateSeller: false, error: action.payload }    
        default:
            return state
    }
}

export default sellersCreate
