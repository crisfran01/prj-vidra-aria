import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import Table from './Table';
import Input from './parts/Input';

import * as usersActions from "../../actions/UserActions"
import * as employActions from "../../actions/EmploysActions"



import Clear from '@material-ui/icons/Clear';
import Search from '@material-ui/icons/Search';
import Save from '@material-ui/icons/Save';


let employColumns = [
  { title: "Codigo", field: "code" },
  { title: "Nome", field: "nome" },
  { title: "CPF", field: "cpf" },
  { title: "E-mail", field: "email" },
];

let usersColumns = [
  { title: "Codigo", field: "code" },
  { title: "Username", field: "username" },
  { title: "Nível", field: "level"},
  { title: "Nome", field: "nome" },
  { title: "Código Funcionário", field: "cod_fun", hidden: true },
];

class User extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      searchUsers: [],
      code: "",
      username: "",
      level: "",
      employ: {
        email: "",
        code: "",
        name: "",
        cpf: "",
      },
    }
   
  }

  componentDidMount() {
    this.props.fetchUsers();
  }


  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextProps.employs !== this.props.employs) {
      let searchEmploys = [];
      let employs = nextProps.employs;
      for(let i = 0; i < Object.keys(employs).length; i ++){
          searchEmploys.push({                
              'nome': employs[i]['nome'],
              'code': employs[i]['cod'],
              'cpf': employs[i]['cpf'],
              'email': employs[i]['email'],
          })
      }
     
      this.setState({
          'searchEmploys' : searchEmploys
      })
    }

    if (nextProps.users !== this.props.users) {
      let searchUsers = [];
      let users = nextProps.users;
      for(let i = 0; i < Object.keys(users).length; i ++){
          searchUsers.push({                
              'username': users[i]['username'],
              'code': users[i]['cod'],
              'level': users[i]['nivel'],
              'nome': users[i]['nome'],
              'cod_fun': users[i]['cod_fun'],

          })
      }
     
      this.setState({
          'searchUsers' : searchUsers
      })
    }


  }

  openEmploy = (event) => {
    event.preventDefault();
    if (this.props.employs.length === 0) {
      this.props.fetchEmploys();
    }
    this.setState({
      showModal: 'employ'
    })
  }


  closeEmploy = () => {
    this.setState({
      showModal: false
    })
  }

  selectEmploy = (event, rowData) => {
    this.setState({
     employ: {
       cod: rowData['code'],
       name: rowData['nome'],
       email: rowData['email'],
       cpf: rowData['cpf'],
     },
     employtSelect: true,
     showModal: false,
   })
  }

   closeModal = () =>{
     this.setState({
      showModal: false,
     })
   }

  

   saveUser = (event) => {
     event.preventDefault();

     console.log(this.state.employ);
     let error = false;
    
    if(this.state.employ.code === ""){
      error = 'Selecione um Funucionário.';
    }else if( this.state.username === ""){
      error = 'Digite um nome de usuário';
    }else if( this.state.level === ""){
      error = 'Escolha um nível';
    }
    if(!error){
     
      let data = {
        'cod': this.state.code,
        'nome': this.state.username,
        'senha': this.state.employ.cpf,
        'nivel': this.state.level,
        'cod_fun': this.state.employ.cod,
      };
  
      this.props.createUser(data);

   
      this.setState({
        code: "",
        username: "",
        level: "",
        employ: {
          email: "",
          code: "",
          name: "",
          cpf: "",
        },
        employtSelect: false,
      })

    }else{
      this.setState({ errorMessage : error })
    }

   }



   changeInput = (name, value) => {
      this.setState({
        [name] : value,
        inputUpdate: true,
      })   
   }

   update = (newData) =>{


    let data = {
      'nome': newData.username,
      'cod_fun': newData.cod_fun,
    };

    this.props.updateUser(data, newData.code);

  }

   delete = (oldData) =>{

    this.props.deleteUser(oldData.code);

   }
  
  

  render() {

    let editable = true;
    
    let modal = '';

    if (this.state.showModal) {
      let table = "";
      if(this.state.searchEmploys){
          table = (
            <Table columns={employColumns}
            data={this.state.searchEmploys}
            title=''
            exportButton={false}
            edit={false}
            exportButton={false}
            onClick={this.selectEmploy}            
          ></Table>
          )
      }else{
        table = <p className='sale-load-icon' >Caregando...</p>
      }
      modal = (
        <div class='modal'>
          <button onClick={this.closeModal} className='close-modal'><Clear>/</Clear></button>
         {table}
        </div>
      )
    }
  
    return (
      <main className='user sell container'>
        <h1>Usuários</h1>
        <div className='sell-cli'>
          <h2>Cadastrar Usuário:</h2>
          <form className='client-form'>
            <p className='cl1'>Funcionário</p>
            <Input className='label-input cl20' id='client'  value={this.state.employ['cod'] || ''} disabled={this.state.employtSelect} type='number' name='code' placeholder='Código'></Input >
            <Input className='label-input cl70' value={this.state.employ['name'] || ''} disabled={this.state.employtSelect} type='text' required name='name' placeholder='Nome'></Input >
            <button onClick={this.openEmploy} className='label-input cl10' ><Search></Search></button>
            <p className='cl1'>Acessos</p>
            <Input onChange={this.changeInput} className='label-input cl40' value={this.state['username'] || ''} type='text' required name='username' placeholder='Username'></Input >
            <Input onChange={this.changeInput} className='label-input cl40' value={this.state['level'] || ''} type='number' required name='level' placeholder='Nivel'></Input >
            <button onClick={this.saveUser} className='sell-item-insert label-input cl20' > Inserir</button >
            <p className='item-error-message'>{this.state.errorMessage}</p>
          </form>
        </div>
        <div className='items-contaner'>
          <Table columns={usersColumns}
            data={this.state.searchUsers}
            title=''
            edit={true}
            exportButton={true}
            pageSize={8}
            showSeeIcon={false}
            changeUpdateData={this.update}
            deleteData={this.delete}
          ></Table>
        </div>

        {modal}
      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchUsers: () => {
    dispatch(usersActions.fetchUsers())
  },
  createUser: (data) => {
    dispatch(usersActions.createUser(data))
  },
  updateUser: (data, code) => {
    dispatch(usersActions.updateUser(data, code))
  },
  deleteUser: (code) => {
    dispatch(usersActions.deleteUser(code))
  },
  fetchEmploys: () => {
    dispatch(employActions.fetchEmploys())
  },
})

const mapStateToProps = state => ({
  users: state.userReducer.users,
  createUser: state.userReducer.createUser,
  employs: state.employReducer.employs
})




export default connect(mapStateToProps, mapDispatchToProps)(User)
