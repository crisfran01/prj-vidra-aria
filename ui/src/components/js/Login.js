import React from 'react';
import { connect } from "react-redux"
import Table from './Table';
import Input from './parts/Input';
import TextArea from './parts/TextArea';

import * as actions from "../../actions/UserActions"
import { login } from "../../services/auth";


class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      username: "",
      pass: "",
    }
   
  }

  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if(nextProps.login == true){
        login('true');
        window.location ='/home';

    }
    

  }


   makeLogin = (event) => {
     event.preventDefault();

     let error = false;
    
    if(this.state.username === ""){
      error = 'Digite um usuário.';
    }

    if(this.state.pass === ""){
        error = 'Digite a senha.';
    }

    if(true){
    
      let data = {
        'senha': this.state.pass,
        'nome': this.state.username,
      };
  
     // this.props.checkLogin(data);

   
      this.setState({
        pass: "",
        username: "",
      })
      login('true');
      window.location ='/home';


    }else{
      this.setState({ errorMessage : error })
    }

   }



   changeInput = (name, value) => {
      this.setState({
        [name] : value,
        inputUpdate: true,
      })   
   }

  
  render() {

    console.log(this.state)
    return (
      <main className='user sell container'>
        <h1> Login</h1>
        <div className='sell-cli'>
          <form method='post' className='login-form'>
            <Input onChange={this.changeInput} value={this.state.parcelas} className='label-input cl40' title='Username'  type='text' name='pass'  required={true}></Input>
            <Input onChange={this.changeInput} value={this.state.data_first} className='label-input cl40' title='Senha'  type='password' name='username'  required={true}></Input>
            <button onClick={this.makeLogin} className='sell-item-insert label-input cl20' > Logar</button >
            <p className='item-error-message'>{this.state.errorMessage}</p>
          </form>
        </div>
       
       

      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
    checkLogin: (data) => {
        dispatch(actions.checkLogin(data))
    }
    
})

const mapStateToProps = state => ({
    login: state.userReducer.login,  
    usersId: state.userReducer.usersId,
})




export default connect(mapStateToProps, mapDispatchToProps)(Login)
