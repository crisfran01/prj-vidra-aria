import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import Table from './Table';
import Input from './parts/Input';
import TextArea from './parts/TextArea';

import * as clientActions from "../../actions/ClientsActions"
import * as prodActions from "../../actions/ProductsActions"
import * as itemActions from "../../actions/ItemsActions"
import * as saleActions from "../../actions/SaleActions"



import Clear from '@material-ui/icons/Clear';
import Search from '@material-ui/icons/Search';
import Save from '@material-ui/icons/Save';


let prodColumns = [
  { title: "Codigo", field: "code" },
  { title: "Nome", field: "nome" },
  { title: "Descrição", field: "descricao" },
  { title: "Preço", field: "pr_venda", type: "numeric" },
];

let clientColumns = [
  { title: "Codigo", field: "code" },
  { title: "Nome", field: "nome" },
  { title: "Tel", field: "tel", type: "numeric" },
  { title: "CPF", field: "cpf", type: "numeric" },
  { title: "Endereço", field: "adress" },
  { title: "E-mail", field: "email" }
];

class Vendas extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      clientSelect: false,
      productSelect: false,
      client:{
        cod: '',
        name: ''
      },
      product:{
        cod:'',
        price: '',
      },
      alt:"",
      lag: "",
      qtde: "",
      metragem: "",
      cod: '',
      cod_fun: "",
      cod_pg: "",
      obs: "",
      productsList: [],
      value: "",
      errorMessage: "",
      itemErrorMessage: ""
    }
   
  }

  componentDidMount() {
    this.props.getSaleNumb()
  }


  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextProps.products !== this.props.products) {
      let searchProducts = [];
      let products = nextProps.products;
      for (let i = 0; i < Object.keys(products).length; i++) {
        searchProducts.push({
          'code': products[i]['cod'],
          'nome': products[i]['nome'],
          'pr_venda': products[i]['pr_venda'],
          'descricao': products[i]['descricao'],
      })
      }

      this.setState({
        'searchProducts': searchProducts
      })
    }


    if (nextProps.clients !== this.props.clients) {
      let searchClients = [];
      let clients = nextProps.clients;
      for (let i = 0; i < Object.keys(clients).length; i++) {
        searchClients.push({
          'nome': clients[i]['nome'],
          'code': clients[i]['cod'],
          'tel': clients[i]['tel'],
          'cpf': clients[i]['cpf'],
          'adress': clients[i]['end'] + ', ' + clients[i]['numb'] + ', ' + clients[i]['bairro'],
          'email': clients[i]['email']
        })
      }
      this.setState({
        'searchClients': searchClients
      })
    }

    if (nextProps.newSale !== this.props.newSale) {

      this.setState({
        'cod': nextProps.newSale
      })
     
    }

    if(nextState.alt !== '' && nextState.qtde !== '' && nextState.lag !== "" && nextState.product['price'] !== '' && nextState.inputUpdate){
      this.setState({
        metragem: nextState.qtde * nextState.alt * nextState.lag,
        value: nextState.qtde * nextState.alt * nextState.lag * nextState.product['price'],
        inputUpdate: false
      })
    }

  }

  openProducts = (event) => {
    event.preventDefault();
    if (this.props.products.length === 0) {
      this.props.fetchProducts();
    }
    this.setState({
      showModal: 'prods'
    })
  }

  openClients = (event) => {
    event.preventDefault();
    if (this.props.clients.length === 0) {
      this.props.fetchClients();
    }
    this.setState({
      showModal: 'clients'
    })
  }

  closeroducts = () => {
    this.setState({
      showModal: false
    })
  }

  closeClients = () => {
    this.setState({
      showModal: false
    })
  }

  selectClient = (event, rowData) => {
    this.setState({
     client: {
       cod: rowData['code'],
       name: rowData['nome']
     },
     clientSelect: true,
     showModal: false,
   })
  }

  selectProduct = (event, rowData) => {
     this.setState({
      product: {
        cod: rowData['code'],
        price: rowData['pr_venda'],
        name: rowData['nome']
      },
      productSelect: true,
      showModal: false,
    })
   }

   closeModal = () =>{
     this.setState({
      showModal: false,
     })
   }

   insertProduct = (event) =>{
    event.preventDefault();

    if(this.state.qtde !== "" && this.state.alt !== "" && this.state.lag !== ""){    
      let products = this.state.productsList;
      products.push({
        "price": this.state.product['price'],
        "code": this.state.product['cod'],
        "name": this.state.product['name'],
        "alt": this.state.alt,
        "lag": this.state.lag,
        "qtde": this.state.qtde,
        "metragem": this.state.metragem,
        "value": this.state.value
      })
      this.setState({
        product:{
          cod:'',
          price: '',
        },
        alt: '',
        lag: "",
        qtde: "",
        metragem: "",
        cod_fun: "",
        cod_pg: "",
        obs: "",
        value: "",
        productSelect: false,
        errorMessage: "",
        itemErrorMessage: "",
      })
    }else{
      this.setState({
        itemErrorMessage: "Preencha todos os campos"
      })
    }


   }

   saveSale = (event) => {
     event.preventDefault();
     let client = document.querySelector('.client-form #client').value;
     let error = false;
     let value = 0;
    
    if(client === ""){
      error = 'Selecione um cliente.';
    }else if( this.state.productsList.length === 0){
      error = 'Adicione pelo menos um produto.';
    }
    if(!error){
      this.state.productsList.forEach((item) => {
        value += parseFloat(item['value'])
      })

      let today = new Date();
      let date = today.getDate() + '/' + (today.getMonth() + 1) + '/' +today.getFullYear();
      
      let data = {
        'valor': value,
        'cod_cli': client,
        'cod_fun': this.state.cod_fun,  
        'cod_pg':  this.state.cod_pg,  
        'obs': this.state.obs,  
        'data': date
      };
  
      this.props.createSale(data);

      let saleCod = this.state.cod;

      this.state.productsList.forEach((item) => {
        let data ={
          "cod_vend": saleCod,
          "cod_prod":item["code"] ,
          "altura":item["alt"],
          "largura":item["lag"],
          "qtde":item["qtde"],
          "metragem":item["metragem"],
          "preco":item["value"],
        }
        this.props.createItem(data);
    
      })

      this.setState({
        showModal: false,
        clientSelect: false,
        productSelect: false,
        client:{
          cod: '',
          name: '',
        },
        alt:"",
        lag: "",
        qtde: "",
        metragem: "",
        cod_fun: "",
        cod_pg: "",
        obs: "",
        value: "",
        product:{
          cod:'',
          price: ''
        },
        productsList: [],
        errorMessage: "",
        itemErrorMessage: ""
      })

    }else{
      this.setState({ errorMessage : error })
    }

   }

   changeUpdateData = (newData, oldData)=>{

    if(oldData['value'] ===  newData['value'] )
        newData['value'] = parseFloat(newData['qtde']) * parseFloat(newData['alt']) * parseFloat(newData['lag']) * parseFloat(newData['price']);

     return newData;
   }


   changeInput = (name, value) => {

    this.setState({
      [name] : value,
      inputUpdate: true,
    })   

   }

   changeValue = (name, value) => {

    this.setState({
      'value' : value,
    })   

   }


  render() {

    let editable = false;
    
    let modal = '';
    if (this.state.showModal) {
      let modalColuns = this.state.showModal === 'clients' ? clientColumns : prodColumns;
      let modalItems = this.state.showModal === 'clients' ? this.state.searchClients : this.state.searchProducts;
      let modalButton = this.state.showModal === 'clients' ? this.selectClient : this.selectProduct;
      let table = "";
      if(modalItems){
          table = (
            <Table columns={modalColuns}
            data={modalItems}
            title=''
            exportButton={false}
            edit={false}
            exportButton={false}
            onClick={modalButton}
          ></Table>
          )
      }else{
        table = <p className='sale-load-icon' >Caregando...</p>
      }
      modal = (
        <div class='modal'>
          <button onClick={this.closeModal} className='close-modal'><Clear>/</Clear></button>
         {table}
        </div>
      )
    }
  

    return (
      <main className='sell container'>
        <h1>Venda {this.state.cod} </h1>
        <div className='sell-cli'>
          <h2>Informações do Cliente</h2>
          <form className='client-form'>
            <Input className='label-input cl20' id='client'  value={this.state.client['cod'] || ''} disabled={this.state.clientSelect} type='number' name='code' placeholder='Código'></Input >
            <Input className='label-input cl70' value={this.state.client['name'] || ''} disabled={this.state.clientSelect} type='text' required name='name' placeholder='Nome'></Input >
            <button onClick={this.openClients} className='label-input cl10' ><Search></Search></button>
          </form>
        </div>
        <div className='sell-prods'>
          <h2>Produtos</h2>
          <form className='product-form'>
            <Input className='label-input cl20' value={this.state.product['cod'] || ''} disabled={this.state.productSelect} type='number' name='code' placeholder='Código'></Input >
            <Input className='label-input cl2' value={this.state.product['name'] || ''} disabled={this.state.productSelect} type='text' required name='name' placeholder='Nome'></Input >
            <Input className='label-input cl20' value={this.state.product['price'] || ''} disabled type='number' name='value' placeholder='Preço'></Input >
            <button onClick={this.openProducts} className='label-input cl10' ><Search></Search></button>
            <Input className='label-input alt cl4' onChange={this.changeInput}  value={this.state.alt || ''} disabled={editable} type='number' id='alt' name='alt' placeholder='Altura'></Input >
            <Input className='label-input lag cl4'  onChange={this.changeInput} value={this.state.lag || ''} disabled={editable} type='number' id='lag' name='lag' placeholder='Largura'></Input >
            <Input className='label-input qtde cl8'  onChange={this.changeInput} value={this.state.qtde || ''} disabled={editable} type='number' id='qtde' name='qtde' placeholder='Quantidade'></Input >
            <Input className='label-input cl4'  onChange={this.changeValue} value={this.state.value || ''} type='number' name='value' placeholder='Valor'></Input >

            <button onClick={this.insertProduct} className='sell-item-insert label-input cl8' > Inserir</button >
          </form>
          <p className='item-error-message'>{this.state.itemErrorMessage}</p>
        </div>
        <div className='items-contaner'>
          <Table columns={[
            { title: "PR", field: "price" },
            { title: "Codigo", field: "code" },
            { title: "Nome", field: "name" },
            { title: "Largura", field: "lag", type: "numeric" },
            { title: "Altura", field: "alt", type: "numeric" },
            { title: "Quantidade", field: "qtde", type: "numeric" },
            { title: "Metragem", field: "metragem", type: "numeric" },
            { title: "Valor", field: "value", type: "numeric" }
          ]}
            data={this.state.productsList}
            title=''
            edit={true}
            exportButton={false}
            updateRow={this.updateRow}
            pageSize={8}
            changeUpdateData = {this.changeUpdateData}

          ></Table>
        </div>

        {modal}

        <div className='sale-form'>
            <TextArea value={this.state.obs} onChange={this.changeInput} className='label-textarea' title='Observações' disabled={false} name='obs'></TextArea>
            <Input className='label-input cl20'  onChange={this.changeInput} value={this.state.cod_fun || ''} disabled={false} type='number' name='cod_fun' placeholder='Funicionário'></Input >
            <Input className='label-input cl20' onChange={this.changeInput}  value={this.state.cod_pg || ''} disabled={false} type='number' name='cod_pg' placeholder='Condição de pagamento'></Input >
            <button className='save-sale' onClick={this.saveSale}><Save></Save></button>
        </div>
        <p className='item-error-message'>{this.state.errorMessage}</p>

      </main>
    );

  }

}

Vendas.propTypes = {
  searchItems: PropTypes.array
}

Vendas.defaultProps = {
  searchItems: [],
}


const mapDispatchToProps = dispatch => ({
  fetchClients: () => {
    dispatch(clientActions.fetchClients())
  },
  fetchProducts: () => {
    dispatch(prodActions.fetchProducts())
  },
  getSaleNumb: () => {
    dispatch(saleActions.getSaleNumb())
  },
  createSale: (data) => {
    dispatch(saleActions.createSale(data))
  },
  createItem: (data) => {
    dispatch(itemActions.createItem(data))
  }
})

const mapStateToProps = state => ({
  clients: state.clientsReducer.clients,
  products: state.productReducer.products,
  newSale: state.saleReducer.newSale,
})




export default connect(mapStateToProps, mapDispatchToProps)(Vendas)
