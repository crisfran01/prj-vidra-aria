import React, {Component} from 'react';
import SimpleLineChart from './SimpleLineChart';
import SimplePizzaChart from './SimplePizzaChart';

class Home extends React.Component{
  render() {
    return (
      <div className='home container'>
        <div className='6-months'>
          <h2 className='home-title'>Vendas dos ultimos 6 meses</h2>
          <SimpleLineChart></SimpleLineChart>
        </div>
        <div className='home-allerts'>
          <div className='home-contas-allert'>
            <h2 className='home-title'>Contas Atrasadas</h2>
            <div className='home-allert'>
              <p><strong>Código: </strong>25</p>
              <p><strong>Cliente: </strong>Roberto Alencar</p>
              <p><strong>Valor: </strong>R$500,00</p>
            </div>
            <div className='home-allert'>
              <p><strong>Código: </strong>25</p>
              <p><strong>Cliente: </strong>Roberto Alencar</p>
              <p><strong>Valor: </strong>R$500,00</p>
            </div>
            <div className='home-allert'>
              <p><strong>Código: </strong>25</p>
              <p><strong>Cliente: </strong>Roberto Alencar</p>
              <p><strong>Valor: </strong>R$500,00</p>
            </div>            
          </div>
          <div className='home-prods-allert'>
            <h2 className='home-title'>Produtos em falta</h2>
            <div className='home-allert'>
              <p><strong>Código: </strong>2</p>
              <p><strong>Nome: </strong>Vidro incolor 3mm</p>
              <p><strong>Quanidade: </strong>1m</p>
            </div>
            <div className='home-allert'>
              <p><strong>Código: </strong>2</p>
              <p><strong>Nome: </strong>Vidro incolor 3mm</p>
              <p><strong>Quanidade: </strong>1m</p>
            </div>
            <div className='home-allert'>
              <p><strong>Código: </strong>2</p>
              <p><strong>Nome: </strong>Vidro incolor 3mm</p>
              <p><strong>Quanidade: </strong>1m</p>
            </div>
          </div>
        </div>

        <div className='most-sale'>
          <h2 className='home-title'>Produtos Mais Vendidos</h2>
          <SimplePizzaChart></SimplePizzaChart>
        </div>
      </div>
    );
  }
}

export default Home;
