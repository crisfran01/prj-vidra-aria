import React from 'react';
import PropTypes from 'prop-types';
import Table from './Table';
import Input from './parts/Input';



let searchColumns = [
  { title: "Cliente", field: "cli" },
  { title: "Código venda", field: "cod_vend" },
  { title: "Valor", field: "value" },
  { title: "Funcionário", field: "fun" },
  { title: "OBS", field: "obs" },
];

class ControleDeCaixa extends React.Component{
  

  constructor(props) {
    super(props)
    this.state = {
      take: "",
      errorMessage: ''
    }
   
  }


  changeInput = (name, value) => {
    this.setState({
      [name] : value,
      inputUpdate: true,
    })   
 }

 takeMoney = (event) => {

   event.preventDefault();

   if(this.state.take != ''){

   }else{
    this.setState({ errorMessage : 'Digite um valor' })
   }

 }


  
    render() {

      return (
        <div className='container caixa'>
            <h1> Controle de Caixa</h1>

          <div  className='search-table'>
            <Table  columns={ this.searchColumns}
            data={ []}
            title=''
            edit={false}
            onClick={this.props.onSelectItem}       
            pageSize={10}       
            showSeeIcon={false}  
            ></Table>          
          </div>
          <div className='caixa-footer'>
            <form className='caixa-take cl50'>
              <Input onChange={this.changeInput} value={this.state.take} className='label-input cl50' title=''  type='number' name='take'  required={true}></Input>
              <button onClick={this.takeMoney} className='sell-item-insert label-input cl50' > Retirar</button >
              <p>{this.state.errorMessage}</p>
            </form>
            
            <p><strong>Valor em caixa:</strong> R$200,00</p>
          </div>
        </div>
      );

    }
    
}

ControleDeCaixa.propTypes = {
 
}

ControleDeCaixa.defaultProps = {
 
}



export default ControleDeCaixa;
