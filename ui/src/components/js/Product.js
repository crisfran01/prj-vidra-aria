import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import ProductForm from './ProductForm';
import Busca from './Busca';



import * as actions from "../../actions/ProductsActions"

// import '../css/Client.css';

let columns = [
    { title: "Codigo", field: "code" },
    { title: "Nome", field: "nome" },
    { title: "Descrição", field: "descricao"},
    { title: "Preço", field: "pr_venda", type: "numeric" },
];


class Product extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      productPosition: -1,
      content: 'show'
    }
  }

  componentDidMount() {
    this.props.fetchProducts()
  }

  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextProps.products !== this.props.products) {
        let searchProducts = [];
        let products = nextProps.products;
        for(let i = 0; i < Object.keys(products).length; i ++){
            searchProducts.push({                
                'code': products[i]['cod'],
                'nome': products[i]['nome'],
                'pr_venda': products[i]['pr_venda'],
                'descricao': products[i]['descricao'],
            })
        }
       
        this.setState({
            'searchProducts' : searchProducts
        })
    }

  }


  ChangeContentSearch = () =>{
    this.setState({
        'content':'search'
    });
  }


  ChangeContentShow = (event, rowData) =>{
      let position = this.getProductPositionByCode(rowData['code']);
    this.setState({
        'content':'show',
        'productPosition':  position
    });
  }

  getProductPositionByCode = (code) => {
    let products = this.props.products;
    let position = -1;
    for(let i = 0; i < Object.keys(products).length; i ++){
        if(products[i]['cod'] === code){
            position = i;
        }
    }

    return position;

  }

  render() {
    let content = this.state.content === 'search' ?  
    <Busca onSelectItem={this.ChangeContentShow} searchColumns={columns} searchItems={this.state.searchProducts || [] }> </Busca> :
    <ProductForm productPosition={this.state.productPosition} products={this.props.products || {}} type='see' onSearch={this.ChangeContentSearch} > </ProductForm>;
    return (
      <main className='container informations products'>
        
        <h1>{this.props.title}</h1>
        {content}

      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchProducts: () => {
    dispatch(actions.fetchProducts())
  }
})

const mapStateToProps = state => ({
  products: state.productReducer.products
})


Product.propTypes = {
  title: PropTypes.string,
}

Product.defaultProps = {
  title: ''
}


export default connect(mapStateToProps, mapDispatchToProps)(Product)

