import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import Input from './parts/Input';
import TextArea from './parts/TextArea';


import * as actions from "../../actions/EmploysActions"

import '../css/ClientForm.css';
import Save from '@material-ui/icons/Save';
import Edit from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';
import Clear from '@material-ui/icons/Clear';
import Search from '@material-ui/icons/Search';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';

class EmployForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      employPosition:this.props.employPosition,
      'cod': "",
      'nome': "",
      'funcao': "",
      'nascimento': "",
      'cpf': "",
      'rg': "",
      'end': "",
      'num': "",
      'bairro': "",
      'cidade': "",
      'uf': "",
      'cep': "",
      'fgts': "",
      'tel': "",
      'email': "",
      'obs': "",
      'type': props.type,
      'editEntity': false,
      'employs': this.props.employs
    }
  }

  componentDidMount() {
    if(this.props.employPosition !== -1){
      this._fillEmploy(this.props.employPosition);
    }
  }


  
  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextState.employPosition !== this.state.employPosition) {
      this._fillEmploy(nextState.employPosition);
    }

    if (nextProps.employPosition !== this.props.employPosition) {
      this._fillEmploy(nextProps.employPosition);
    }

    if (nextProps.createSuccess === true && this.state.type !== 'see'){
      this.setState({
        'type': 'see'
      });
    }

    if (nextProps.updateSuccess === true && this.state.type !== 'see'){
      this.setState({
        'type': 'see'
      });
    }

  }

  _fillEmploy = (position) =>{
    let employ = this.props.employs[position];
    console.log(employ)
    this.setState({
      'cod': employ.cod.toString(),
      'nome': employ.nome || '',
      'funcao': employ.funcao || '',
      'nascimento': employ.nascimento || '',
      'cpf': employ.cpf || '',
      'rg': employ.rg || '',
      'end': employ.end || '',
      'num': employ.num || '',
      'bairro': employ.bairro || '',
      'cidade': employ.cidade || '',
      'uf': employ.uf || '',
      'cep': employ.cep || '',
      'fgts': employ.fgts || '',
      'tel': employ.tel || '',
      'email': employ.email || '',
      'obs': employ.obs || '',
      'prev': false,
      'next': true
    })
  }



  PrevEmploy = () => {
    console.log('a')
    let position = this.state.employPosition - 1 >= 0 ? this.state.employPosition - 1 : this.state.employPosition;
    console.log(position)

    this.setState({
      employPosition: position
    });
  }

  NextEmploy = (lenght) => {
    let position = this.state.employPosition + 1 < lenght ? this.state.employPosition + 1 : this.state.employPosition;
    this.setState({
      employPosition: position,
    });
  }

  Edit = () => {
    this.setState({
      'type': 'register',
      'editEntity': true
    })
  }

  Cancel = () => {

    this.clearMessages();
    this._fillEmploy(this.state.employPosition);
    this.setState({
      'type': 'see',
      'editEntity': false
    })
    
  }

  Save = () => {
    let data = {};
    let inputs = document.querySelectorAll('.information-data input');
    let selects= document.querySelectorAll('.information-data select');
    let textarea= document.querySelectorAll('.information-data textarea');
    let error = false;
    let cpf = document.getElementsByName('cpf')[0];


    this.clearMessages();

    inputs.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })

    selects.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })

    textarea.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })

    if(!this.validadePersonDocument(cpf.value)){
      error = "true";
      this.createErrorMessage(cpf.parentElement, "CPF Inválido");
    }

    if(!error){
      let code = data['cod'];
      data = JSON.stringify(data); 
  
      if(this.state.editEntity){
        this.props.updateEmploy(data, code);
      }else{
        this.props.createEmploys(data)
      }

    }

  }

  Add = (lenght) =>{

    let cod = this.props.employs[lenght - 1]['cod'] + 1

    this.setState({
      'cod': cod,
      'nome': "",
      'funcao': "",
      'nascimento': "",
      'cpf': "",
      'rg': "",
      'end': "",
      'num': "",
      'bairro': "",
      'cidade': "",
      'uf': "",
      'cep': "",
      'fgts': "",
      'tel': "",
      'email': "",
      'obs': "",
      'type': 'register',
      'editEntity': false
    })

  }

validadePersonDocument = (strCPF) => {
  strCPF = strCPF.replace("-", "").replace("/", "").split('.').join('');
  if (strCPF === "00000000000000" || 
      strCPF === "11111111111111" || 
      strCPF === "22222222222222" || 
      strCPF === "33333333333333" || 
      strCPF === "44444444444444" || 
      strCPF === "55555555555555" || 
      strCPF === "66666666666666" || 
      strCPF === "77777777777777" || 
      strCPF === "88888888888888" || 
      strCPF === "99999999999999" ||
      strCPF === "")
      return false;
  return this.state.person === 'juridica' ? this.validadeCnpj(strCPF) : this.validadeCpf(strCPF);
  
}

validadeCpf =(strCPF) =>{
  let sum = 0;
  for (let i=1; i<=9; i++){
    sum = sum + parseInt(strCPF.substring(i-1, i)) * (11 - i);
  } 

  let rest = (sum * 10) % 11;
   
  if ((rest === 10) || (rest === 11))  
    rest = 0;

  if (rest !== parseInt(strCPF.substring(9, 10)) ) 
    return false;
   
  sum = 0;
  for (let i = 1; i <= 10; i++){
    sum = sum + parseInt(strCPF.substring(i-1, i)) * (12 - i);
  } 

  rest = (sum * 10) % 11;
  
  if ((rest === 10) || (rest === 11))  
    rest = 0;

  if (rest !== parseInt(strCPF.substring(10, 11) ) ) 
    return false;
  
  return true;

}


validadeCnpj =(cnpj) =>{
      
    if (cnpj.length !== 14)
        return false;
    
    let tamanho = cnpj.length - 2
    let numeros = cnpj.substring(0,tamanho);
    let digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado !== digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;

    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }

    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

    if (resultado !== digitos.charAt(1))
          return false;
           
    return true;
    

}

  createErrorMessage = (parent, message) => {
    let span = document.createElement('span');
    span.classList.add('error-message');
    span.innerHTML = message;
    parent.appendChild(span);
  }

  clearMessages = () =>{
    let messages= document.querySelectorAll('.error-message');
    messages.forEach((message) => {
     message.remove();
    })
  }


  changePerson = (event) =>{
    let value = event.target.value;
    this.setState({
      'person': value
    })
  }

  render() {
    if (Object.keys(this.props.employs).length > 0 && this.state.employPosition === -1) {
      this.setState({
        employPosition: 0,
      })
    } 


    let disableNext = this.state.employPosition === Object.keys(this.props.employs).length - 1 ? 'disabled' : '';
    let disablePrev = this.state.employPosition === 0 ? 'disabled' : '';

    let cpfTitle = this.state.person === 'juridica' ? 'CNPJ' : 'CPF';
    let cpfMask = this.state.person === 'juridica' ? '99.999.999/9999-99' : '999.999.999-99';
    let rgTitle = this.state.person === 'juridica' ? 'IE' : 'RG';
    let rgMask = this.state.person === 'juridica' ? '999.999.999.999' : '99.999.999-*';

    let controlsOptionsSee = (
      <div className="navegation-controls">
        <button className={'navegation-button prev ' + disablePrev} onClick={this.PrevEmploy}> <ChevronLeft></ChevronLeft> </button>
        <button className='edit-button' onClick={this.Edit}> <Edit></Edit> </button>
        <button className='add-button' onClick={() => this.Add(Object.keys(this.props.employs).length)}> <Add></Add> </button>
        <button className='search-button' onClick={this.props.onSearch}> <Search></Search> </button>
        <button className={'navegation-button next ' + disableNext} onClick={() => this.NextEmploy(Object.keys(this.props.employs).length)}> <ChevronRight></ChevronRight> </button>

      </div>
    )

    let controlsOptionsRegister = (
      <div className="navegation-controls">
        <button className='cancel-button' onClick={this.Cancel}> <Clear></Clear> </button>
        <button className='save-button' onClick={this.Save}> <Save></Save> </button>
      </div>
    );

    let editable = this.state.type === 'register' ? false : true;
    let controls = this.state.type === 'register' ? controlsOptionsRegister : controlsOptionsSee;
    return (
        <div className="information-data">
          <form action='#' method='POST'>
            <Input value={this.state.cod} className='label-input cl3' title='Código' disabled={true} type='number' name='cod' ></Input>
            <Input value={this.state.funcao} className='label-input cl23' title='Função' disabled={editable} type='text' name='funcao'  required={true}></Input>
            <Input value={this.state.nome} className='label-input cl23' title='Nome' disabled={editable} type='text' name='nome'  required={true}></Input>
            <Input value={this.state.nascimento} className='label-input cl3' title='Data Nasc'  mask='99/99/9999' disabled={editable} type='text' name='nascimento' ></Input>
            <Input value={this.state.email} className='label-input cl23' title='E-mail' disabled={editable} type='email' name='email' ></Input>
            <Input value={this.state.tel} className='label-input cl3' title='Telefone' mask='(99) 9999-99999' disabled={editable} type='tel' name='tel' ></Input>
            <Input value={this.state.fgts} className='label-input cl3' title='FGTS' mask='(99) 9999-99999' disabled={editable} type='text' name='fgts' ></Input>
            <Input value={this.state.cpf} className='label-input cl3' title={cpfTitle}  mask={cpfMask}  disabled={editable} type='text' name='cpf'  required={true}></Input>
            <Input value={this.state.rg} className='label-input cl3' title={rgTitle}  mask={rgMask} disabled={editable} type='text' name='rg' ></Input>
            <Input value={this.state.end} className='label-input cl23' title='Endereço' disabled={editable} type='text' name='end' ></Input>
            <Input value={this.state.num} className='label-input cl3' title='Número' disabled={editable} type='number' name='num' ></Input>
            <Input value={this.state.bairro} className='label-input cl3' title='Bairro' disabled={editable} type='text' name='bairro' ></Input>
            <Input value={this.state.cidade} className='label-input cl3' title='Cidade' disabled={editable} type='text' name='cidade' ></Input>
            <Input value={this.state.cep} className='label-input cl3' title='CEP' mask='99.999-999' disabled={editable} type='text' name='cep' ></Input>
            <TextArea value={this.state.obs}  className='label-textarea' title='Observações' disabled={editable} name='obs'></TextArea>

          </form>

          {controls}

        </div>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchEmploys: () => {
    dispatch(actions.fetchEmploys())
  },
  createEmploys: (data) => {
    dispatch(actions.createEmploy(data))
  },
  updateEmploy: (data, code) => {
    dispatch(actions.updateEmploy(data, code))
  }
})

const mapStateToProps = state => ({
  updateSuccess: state.employReducer.updateEmploy,  
  createSuccess: state.employReducer.createEmploy,  
})


EmployForm.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  onSearch: PropTypes.func,
  employPosition: PropTypes.number,
  employs: PropTypes.array
}

EmployForm.defaultProps = {
  title: '',
  type: 'see',
  employPosition: -1
}


export default connect(mapStateToProps, mapDispatchToProps)(EmployForm)

