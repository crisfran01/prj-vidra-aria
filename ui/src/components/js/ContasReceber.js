import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import Table from './Table';
import Input from './parts/Input';

//import * as contasRecebersActions from "../../actions/ContasReceberActions"
//import * as employActions from "../../actions/EmploysActions"



let contasRecebersColumns = [
  { title: "Codigo Conta", field: "codeC",  hidden: true },
  { title: "Codigo", field: "code" },
  { title: "Cliente", field: "client" },
  { title: "Data", field: "date"},
  { title: "Valor", field: "value" },
];


let contasRecebersData= [];

contasRecebersData.push({                
  'codeC':'1',
  'code':'1',
  'client':'1',
  'data':'1',
  'value':'1',
})


class ContasReceber extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      searchContasReceber: [],
      type: "",
      value: "",
      codeC: ''
    }
   
  }

  componentDidMount() {
    //this.props.fetchContasRecebers();
  }


  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

  

    if (nextProps.contasReceber !== this.props.contasReceber) {
      let searchContasReceber = [];
      let contasRecebers = nextProps.contasRecebers;
      for(let i = 0; i < Object.keys(contasRecebers).length; i ++){
          searchContasReceber.push({                
              'codeC': contasRecebers[i][''],
              'code': contasRecebers[i]['cod'],
              'client': contasRecebers[i][''],
              'data': contasRecebers[i][''],
              'value': contasRecebers[i][''],

          })
      }
     
      this.setState({
          'searchContasReceber' : searchContasReceber
      })
    }


  }

  /*openEmploy = (event) => {
    event.preventDefault();
    if (this.props.employs.length === 0) {
      this.props.fetchEmploys();
    }
    this.setState({
      showModal: 'employ'
    })
  }


  closeEmploy = () => {
    this.setState({
      showModal: false
    })
  }

  selectEmploy = (event, rowData) => {
    this.setState({
     employ: {
       cod: rowData['code'],
       name: rowData['nome'],
       email: rowData['email'],
       cpf: rowData['cpf'],
     },
     employtSelect: true,
     showModal: false,
   })
  }

   closeModal = () =>{
     this.setState({
      showModal: false,
     })
   }*/

  

   pgContasReceber = (data) => {



   }

   searchContas = () => {

   }



   changeInput = (name, value) => {
      this.setState({
        [name] : value,
      })   
   }
  

  render() {

    let editable = true;
    
    let modal = '';
    /*
    if (this.state.showModal) {
      let table = "";
      if(this.state.searchEmploys){
          table = (
            <Table columns={employColumns}
            data={this.state.searchEmploys}
            title=''
            exportButton={false}
            edit={false}
            exportButton={false}
            onClick={this.selectEmploy}            
          ></Table>
          )
      }else{
        table = <p className='sale-load-icon' >Caregando...</p>
      }
      modal = (
        <div class='modal'>
          <button onClick={this.closeModal} className='close-modal'><Clear>/</Clear></button>
         {table}
        </div>
      )
    }*/
  
    return (
      <main className='contasReceber sell container'>
        <h1>Contas a Receber</h1>
        <div className='sell-cli'>
          <form className='client-form'>
            <label className='label-select cl30' >
              <select className='form-select'>
                <option value='ved'> Código venda</option>
                <option value='cli'> Cliente</option>
                <option value='cpf'> CPF </option>
                <option value='date'> Data(dd/mm/yyyy) </option>
              </select>
            </label>
            <Input onChange={this.changeInput} className='label-input cl50' value={this.state['value'] || ''} type='text' required name='value' placeholder='buscar...'></Input >
            <button onClick={this.searchContas} className='sell-item-insert label-input cl20' > Buscar</button >
          </form>
        </div>
        <div className='items-contaner'>
          <Table columns={contasRecebersColumns}
            data={this.state.searchContasReceber}
            title=''
            edit={false}
            exportButton={true}
            pageSize={8}
            showSeeIcon={false}
            showMoneyIcon={true}
            onClick={this.pgContasReceber}
          ></Table>
        </div>

        {modal}
      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
 /* fetchContasRecebers: () => {
    dispatch(contasRecebersActions.fetchContasRecebers())
  },
  createContasReceber: (data) => {
    dispatch(contasRecebersActions.createContasReceber(data))
  },
  updateContasReceber: (data, code) => {
    dispatch(contasRecebersActions.updateContasReceber(data, code))
  },
  deleteContasReceber: (code) => {
    dispatch(contasRecebersActions.deleteContasReceber(code))
  },
  fetchEmploys: () => {
    dispatch(employActions.fetchEmploys())
  },*/
})

const mapStateToProps = state => ({/*
  contasReceber: state.contasReceberReducer.contasReceber,
  createContasReceber: state.contasReceberReducer.createContasReceber,
  employs: state.employReducer.employs*/
})




export default connect(mapStateToProps, mapDispatchToProps)(ContasReceber)
