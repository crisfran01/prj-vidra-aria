import React from 'react';
import PropTypes from 'prop-types';
import '../../css/Input.css';
import InputMask from 'react-input-mask';


class Input extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
       'value': ''
      }
    }

    UNSAFE_componentWillUpdate = (nextProps) => {

      if (nextProps.value !== this.props.value) {
        this.setState({
          'value': nextProps.value
        })
      }
  
    }

    updateValue = event =>{
      this.setState({value: event.target.value});
      if(this.props.onChange){
        this.props.onChange(this.props.name, event.target.value);
      }
    }


    render() {

      let editable = this.props.disabled === false ? "" : "disabled";
      let required = this.props.required === false ? "" : "required";
      let title = this.props.title === "" ? "" : (<span>{this.props.title}</span>);

      return (
        <label className={this.props.className}>
            {title}
            <InputMask id={this.props.id} mask={this.props.mask} onChange={this.updateValue} value={this.state.value} disabled={editable} type={this.props.type} name={this.props.name} placeholder={this.props.placeholder} required={required}></InputMask>
        </label>
      );

    }
    
}



Input.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  id: PropTypes.string,
	placeholder: PropTypes.string,
	title: PropTypes.string,
	className: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  mask: PropTypes.string,
  onChange: PropTypes.func
}

Input.defaultProps = {
  type:'text',
	placeholder:'',
	title:'',
  value:'',
  id:'',
	className:'',
  disabled: false,
  required: false,
  onChange: null
}


export default Input;
