import React from 'react';
import PropTypes from 'prop-types';
// import '../../css/Select.css';


class Select extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
       'value': ''
      }
    }

    UNSAFE_componentWillUpdate = (nextProps) => {

      if (nextProps.value !== this.props.value) {
        this.setState({
          'value': nextProps.value
        })
      }
  
    }

    updateValue = event =>{
      this.setState({value: event.target.value});
    }


    render() {

      let editable = this.props.disabled === false ? "" : "disabled";
      let required = this.props.required === false ? "" : "required";
      let title = this.props.title === "" ? "" : (<span>{this.props.title}</span>);
      let placeholder = this.props.placeholder ? this.props.placeholder : 'Selecione';
      return (
        <label className={this.props.className}>
            {title}
            <select  required={required}  name={this.props.name} disabled={editable} value={this.state.value} onChange={this.props.onChange || this.updateValue}>
              <option disable='disable' value=''>{placeholder}</option>
              {
                this.props.options.map( item => {
                  return (<option key={item['value']} value={item['value']}>{item['label']}</option> )
                })
              }
            </select>  
        </label>
      );

    }
    
}



Select.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
	placeholder: PropTypes.string,
	title: PropTypes.string,
	className: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  onChange: PropTypes.func,
}

Select.defaultProps = {
	placeholder:'',
	title:'',
	value:'',
	className:'',
  disabled: false,
  required: false,
}


export default Select;
