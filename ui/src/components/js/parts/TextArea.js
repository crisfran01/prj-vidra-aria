import React from 'react';
import PropTypes from 'prop-types';
import '../../css/TextArea.css';

class TextArea extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
       'value': props.value
      }
    }

    UNSAFE_componentWillUpdate = (nextProps) => {

      if (nextProps.value !== this.props.value) {
        this.setState({
          'value': nextProps.value
        })
      }
  
    }

    updateValue = event =>{
      this.setState({value: event.target.value});
      if(this.props.onChange){
        this.props.onChange(this.props.name, event.target.value);
      }
    }


    render() {

      let editable = this.props.disabled === false ? "" : "disabled";
      let required = this.props.required === false ? "" : "required";
      let title = this.props.title === "" ? "" : (<span>{this.props.title}</span>);

      return (
        <label className={this.props.className}>
            {title}
            <textarea value={this.state.value} onChange={this.updateValue} disabled={editable} name={this.props.name} placeholder={this.props.placeholder} required={required}></textarea>
        </label>
      );

    }
    
}



TextArea.propTypes = {
  name: PropTypes.string,
	placeholder: PropTypes.string,
	title: PropTypes.string,
	className: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  onChange: PropTypes.func
}

TextArea.defaultProps = {
	placeholder:'',
	title:'',
	className:'',
  disabled: false,
  required: false,
  onChange: null
}


export default TextArea;
