import React, { forwardRef } from "react";
import MaterialTable from "material-table";
import PropTypes from 'prop-types';

 
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import AttachMoney from '@material-ui/icons/AttachMoney';



 
const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

class Table extends React.Component {

  
  state = {
    data : this.props.data
  }

  
  render() {

    var editable = this.props.edit ? { onRowDelete: oldData =>
      new Promise((resolve) => {
          setTimeout(() => {
              {
                  let data = this.state.data;
                  const index = data.indexOf(oldData);
                  data.splice(index, 1);
                  if(this.props.deleteData){
                    this.props.deleteData(oldData);
                  }
                  this.setState({ data }, () => resolve()); 
              }
              resolve();
          }, 1000);
      }),
      onRowUpdate: (newData, oldData) =>
      new Promise((resolve, reject) => {
          setTimeout(() => {
              {
                  const data = this.state.data;
                  const index = data.indexOf(oldData);
                  data[index] = newData;                
                  this.setState({ data }, () => resolve());
                  if(this.props.changeUpdateData){
                    newData = this.props.changeUpdateData(newData, oldData);
                  }
              }
              resolve();
          }, 1000);
      })
    } : {};

    let see = []
    if(this.props.showSeeIcon){
      see =[
        {
          icon: '👁',
          tooltip: 'Visualizar',
          onClick: (event, rowData) => {
            this.props.onClick(event, rowData)
          }
        }
      ]
    }

    if(this.props.showMoneyIcon){
      see =[
        {
          icon: <AttachMoney></AttachMoney>,
          tooltip: 'Receber',
          onClick: (event, rowData) => {
            this.props.onClick(event, rowData)
          }
        }
      ]
    }
  
      return (
        <MaterialTable
          icons={tableIcons}
          columns={this.props.columns}
          data={this.props.data}
          title={this.props.title}
          editable={editable}
          actions={see}
          options={{
            exportButton: this.props.exportButton,
            pageSize: this.props.pageSize,
            pageSizeOptions:[15, 20, 25]
          }}
          localization={{
            pagination: {
                labelDisplayedRows: '{from}-{to} de {count}',
                labelRowsSelect: 'Linhas',
                labelRowsPerPage: 'Linhas por página',
                previousAriaLabel: 'Página anterior',
                previousTooltip: 'Página anterior',
                nextAriaLabel: 'Proxima página',
                nextTooltip: 'Proxima página',
                firstAriaLabel:'Primeira página',
                firstTooltip:'Primeira página',
                lastAriaLabel:'Última página',
                lastTooltip:'Última página',
            },
            header: {
                actions: 'Ver'
            },
            body: {
                emptyDataSourceMessage: 'Sem dados',
                filterRow: {
                    filterTooltip: 'Filtrar'
                },
                editRow:{
                  deleteText: "Tem certeza que deseja Deletar?"
                }
            },
            toolbar:{
              searchPlaceholder: 'Buscar',
              searchTooltip: 'Buscar',
            }

        }}
        />
      );
  
    }
  }


Table.propTypes = {
	columns: PropTypes.array,
	data: PropTypes.array,
  title: PropTypes.string,
  edit: PropTypes.bool,
  exportButton: PropTypes.bool,
  onClick: PropTypes.func,
  changeUpdateData: PropTypes.func,
  deleteData: PropTypes.func,
  pageSize: PropTypes.number,
  showSeeIcon: PropTypes.bool,
  showSeeIcon: PropTypes.bool,

}

Table.defaultProps = {
  exportButton: true,
  onClick: null,
  changeUpdateData: null,
  pageSize: 15,
  showSeeIcon: true,
  deleteData: null,
  showSeeIcon: false
}


export default Table;