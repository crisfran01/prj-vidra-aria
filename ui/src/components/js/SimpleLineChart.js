import React, { Component } from 'react';
import {
  ResponsiveContainer, ComposedChart, Line, Area, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
  {
    name: '05', Lucros: 590, pv: 800, Vendas: 1400,
  },
  {
    name: '06', Lucros: 868, pv: 967, Vendas: 1506,
  },
  {
    name: '07', Lucros: 1397, pv: 1098, Vendas: 989,
  },
  {
    name: '08', Lucros: 1480, pv: 1200, Vendas: 1228,
  },
  {
    name: '09', Lucros: 1520, pv: 1108, Vendas: 1100,
  },
  {
    name: '10', Lucros: 1400, pv: 680, Vendas: 1700,
  },
];

class SimpleLineChart extends Component {
  
    render() {
        return (
          <div style={{ width: '100%', height: 300 }}>
            <ResponsiveContainer>
              <ComposedChart
                width={500}
                height={400}
                data={data}
                margin={{
                  top: 20, right: 20, bottom: 20, left: 0,
                }}
              >
                <CartesianGrid stroke="#f5f5f5" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Area type="monotone" dataKey="Vendas" fill="#8884d8" stroke="#8884d8" />
                <Line type="monotone" dataKey="Lucros" stroke="#ff7300" />
              </ComposedChart>
            </ResponsiveContainer>
          </div>
        );
      }
}

export default SimpleLineChart;