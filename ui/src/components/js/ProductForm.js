import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import Input from './parts/Input';
import TextArea from './parts/TextArea';


import * as actions from "../../actions/ProductsActions"

import '../css/ClientForm.css';

import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Save from '@material-ui/icons/Save';
import Edit from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';
import Clear from '@material-ui/icons/Clear';
import Search from '@material-ui/icons/Search';



class ProductForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      productPosition:this.props.productPosition,
      'cod': "",
      'nome': "",
      'unid_med': "",
      'pr_custo': "",
      'pr_venda': "",
      'qtde': "",
      'descricao': "",
      'type': props.type,
      'editEntity': false,
      'products': this.props.products
    }
  }

  componentDidMount() {
    if(this.props.productPosition !== -1){
      this._fillProduct(this.props.productPosition);
    }
  }


  
  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextState.productPosition !== this.state.productPosition) {
      this._fillProduct(nextState.productPosition);
    }

    if (nextProps.productPosition !== this.props.productPosition) {
      this._fillProduct(nextProps.productPosition);
    }

    if (nextProps.createSuccess === true && this.state.type !== 'see'){
      this.setState({
        'type': 'see'
      });
    }

    if (nextProps.updateSuccess === true && this.state.type !== 'see'){
      this.setState({
        'type': 'see'
      });
    }

  }

  _fillProduct = (position) =>{
    let product = this.props.products[position];
    this.setState({
      'cod': product.cod.toString(),
      'nome': product.nome || '',
      'unid_med': product.unid_med || '',
      'pr_custo': product.pr_custo || '',
      'pr_venda': product.pr_venda || '',
      'qtde': product.qtde || '',
      'descricao': product.descricao || '',
      'prev': false,
      'next': true
    })
  }



  PrevProduct = () => {
    let position = this.state.productPosition - 1 >= 0 ? this.state.productPosition - 1 : this.state.productPosition;
    this.setState({
      productPosition: position
    });
  }

  NextProduct = (lenght) => {
    let position = this.state.productPosition + 1 < lenght ? this.state.productPosition + 1 : this.state.productPosition;
    this.setState({
      productPosition: position,
    });
  }

  Edit = () => {
    this.setState({
      'type': 'register',
      'editEntity': true
    })
  }

  Cancel = () => {

    this.clearMessages();
    this._fillProduct(this.state.productPosition);
    this.setState({
      'type': 'see',
      'editEntity': false
    })
    
  }

  Save = () => {
    let data = {};
    let inputs = document.querySelectorAll('.information-data input');
    let textarea= document.querySelectorAll('.information-data textarea');
    let error = false;


    this.clearMessages();

    inputs.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })


    textarea.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })

    if(!error){
      let code = data['cod'];
      data = JSON.stringify(data); 
  
      if(this.state.editEntity){
        this.props.updateProduct(data, code);
      }else{
        this.props.createProducts(data)
      }

    }

  }

  Add = (lenght) =>{

    let cod = this.props.products[lenght - 1]['cod'] + 1

    this.setState({
      'cod': cod,
      'nome': "",
      'unid_med': "",
      'pr_custo': "",
      'pr_venda': "",
      'qtde': "",
      'descricao': "",
      'type': 'register',
      'editEntity': false
    })

  }

  createErrorMessage = (parent, message) => {
    let span = document.createElement('span');
    span.classList.add('error-message');
    span.innerHTML = message;
    parent.appendChild(span);
  }

  clearMessages = () =>{
    let messages= document.querySelectorAll('.error-message');
    messages.forEach((message) => {
     message.remove();
    })
  }


  changePerson = (event) =>{
    let value = event.target.value;
    this.setState({
      'person': value
    })
  }

  render() {
    if (Object.keys(this.props.products).length > 0 && this.state.productPosition === -1) {
      this.setState({
        productPosition: 0,
      })
    } 


    let disableNext = this.state.productPosition === Object.keys(this.props.products).length - 1 ? 'disabled' : '';
    let disablePrev = this.state.productPosition === 0 ? 'disabled' : '';

    let controlsOptionsSee = (
      <div className="navegation-controls">
        <button className={'navegation-button prev ' + disablePrev} onClick={this.PrevProduct}> <ChevronLeft></ChevronLeft> </button>
        <button className='edit-button' onClick={this.Edit}> <Edit></Edit> </button>
        <button className='add-button' onClick={() => this.Add(Object.keys(this.props.products).length)}> <Add></Add> </button>
        <button className='search-button' onClick={this.props.onSearch}> <Search></Search> </button>
        <button className={'navegation-button next ' + disableNext} onClick={() => this.NextProduct(Object.keys(this.props.products).length)}> <ChevronRight></ChevronRight> </button>

      </div>
    )

    let controlsOptionsRegister = (
      <div className="navegation-controls">
        <button className='cancel-button' onClick={this.Cancel}> <Clear></Clear> </button>
        <button className='save-button' onClick={this.Save}> <Save></Save> </button>
      </div>
    );

    let editable = this.state.type === 'register' ? false : true;
    let controls = this.state.type === 'register' ? controlsOptionsRegister : controlsOptionsSee;
    return (
        <div className="information-data">
          <form action='#' method='POST'>
            <Input value={this.state.cod} className='label-input cl3' title='Código' disabled={true} type='number' name='cod' ></Input>
            <Input value={this.state.nome} className='label-input cl3' title='Nome' disabled={editable} type='text' name='nome'  required={true}></Input>
            <Input value={this.state.unid_med} className='label-input cl3' title='Unidade de medida' disabled={editable} type='text' name='unid_med' ></Input>
            <Input value={this.state.pr_custo} className='label-input cl3' title='Preço de Custo'   disabled={editable} type='text' name='pr_custo' ></Input>
            <Input value={this.state.pr_venda} className='label-input cl3' title='Preço de Venda'  disabled={editable} type='text' name='pr_venda' ></Input>
            <Input value={this.state.qtde} className='label-input cl3' title='Quantidade' disabled={editable} type='text' name='qtde' ></Input>
            <TextArea value={this.state.descricao}  className='label-textarea' title='Observações' disabled={editable} name='descricao'></TextArea>

          </form>

          {controls}

        </div>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchProducts: () => {
    dispatch(actions.fetchProducts())
  },
  createProducts: (data) => {
    dispatch(actions.createProduct(data))
  },
  updateProduct: (data, code) => {
    dispatch(actions.updateProduct(data, code))
  }
})

const mapStateToProps = state => ({
  updateSuccess: state.productReducer.updateProduct,  
  createSuccess: state.productReducer.createProduct,  
})


ProductForm.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  onSearch: PropTypes.func,
  productPosition: PropTypes.number,
  products: PropTypes.array
}

ProductForm.defaultProps = {
  title: '',
  type: 'see',
  productPosition: -1
}


export default connect(mapStateToProps, mapDispatchToProps)(ProductForm)

