import React from 'react';
import { connect } from "react-redux"
import Busca from './Busca';
import Input from './parts/Input';
import TextArea from './parts/TextArea';

import * as actions from "../../actions/SaleActions"
import * as itemActions from "../../actions/ItemsActions"

let columns = [
    { title: "Codigo", field: "code" },
    { title: "Data", field: "date" },
    { title: "Cliente", field: "nome" },
    { title: "Valor", field: "value", type: "numeric" }
];


class ShowSale extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            salePosition: -1,
            content: 'show',
            saleItems: []
        }
    }

    componentDidMount() {
        this.props.fetchSale()
    }

    UNSAFE_componentWillUpdate = (nextProps, nextState) => {

        if (nextProps.sales !== this.props.sales) {
            let allSales = [];
            let sales = nextProps.sales;
            for (let i = 0; i < Object.keys(sales).length; i++) {
                allSales.push({
                    'nome': sales[i]['nome'],
                    'date': sales[i]['data'],
                    'code': sales[i]['cod'],
                    'value': sales[i]['valor']
                })
            }

            this.setState({
                'allSales': allSales
            })
        }

        if (nextProps.items !== this.props.items) {
            let saleItems = [];
            let items = nextProps.items;
            for (let i = 0; i < Object.keys(items).length; i++) {
                saleItems.push({
                    'code': items[i]['cod_prod'],
                    'name': items[i]['nome'],
                    'alt': items[i]['altura'],
                    'lag': items[i]['largura'],
                    'qtde': items[i]['qtde'],
                    'metragem': items[i]['metragem'],
                    'value': items[i]['preco']
                })
            }

            this.setState({
                'saleItems': saleItems
            })
        }

    }


    ChangeContentSearch = () => {
        this.setState({
            'content': 'search'
        });
    }


    ChangeContentShow = (event, rowData) => {
        let position = this.getSalePositionByCode(rowData['code']);
        this.props.fetchItems(rowData['code']);
        this.setState({
            'content': 'show',
            'selectSale' : 
                {
                    'name': this.props.sales[position]['nome'],
                    'code': this.props.sales[position]['cod'],
                    'cod_cli': this.props.sales[position]['cod_cli'],
                    'cod_pg': this.props.sales[position]['cod_pg'],
                    'cod_fun': this.props.sales[position]['cod_fun'],
                    'obs': this.props.sales[position]['obs'],
                }
        });
    }

   

    getSalePositionByCode = (code) => {
        let sales = this.props.sales;
        let position = -1;
        for (let i = 0; i < Object.keys(sales).length; i++) {
            if (sales[i]['cod'] === code) {
                position = i;
            }
        }

        return position;

    }

    render() {

        let saleModal = '';
        let overlay = '';

        if(this.state.content === 'show' && this.state.selectSale){
            console.log('f')
            console.log(this.state.selectSale['cod_fun'])

            saleModal =(
                <div className='sale-modal'>

                <p onClick={this.ChangeContentSearch} className='close-sale'>x</p>

                    <h2>Venda {this.state.selectSale['code']}</h2>
                    
                    <div className='flex-space-between'>
                        <input className='label-input cl4' id='client'  value={this.state.selectSale['cod_cli']} disabled type='number' name='code' placeholder='Código'></input>
                        <input className='label-input cl34' value={this.state.selectSale['name']} disabled type='text' required name='name' placeholder='Nome'></input >
                    </div>
                

                    <Busca 
                        searchColumns={[
                            { title: "Codigo", field: "code" },
                            { title: "Nome", field: "name" },
                            { title: "Largura", field: "lag", type: "numeric" },
                            { title: "Altura", field: "alt", type: "numeric" },
                            { title: "Quantidade", field: "qtde", type: "numeric" },
                            { title: "Metragem", field: "metragem", type: "numeric" },
                            { title: "Valor", field: "value", type: "numeric" }
                        ]} 
                        showSeeIcon={false}
                        pageSize={10}
                        searchItems={this.state.saleItems || []}> 
                    </Busca>

                    <div className='sale-form'>
                        <TextArea value={this.state.selectSale['obs']} onChange={this.changeInput} className='label-textarea' title='Observações' disabled={true} name='obs'></TextArea>
                        <input className='label-input cl20'  onChange={this.changeInput} value={this.state.selectSale['cod_fun']} disabled={true} type='number' name='cod_fun' placeholder='Funicionário'></input >
                        <input className='label-input cl20' onChange={this.changeInput}  value={this.state.selectSale['cod_pg']} disabled={true} type='number' name='cod_pg' placeholder='Condição de pagamento'></input >
                    </div>
                </div>
            )

            overlay = <span onClick={this.ChangeContentSearch} className='overlay'></span>;

        }

        let content = <Busca onSelectItem={this.ChangeContentShow} searchColumns={columns} searchItems={this.state.allSales || []}> </Busca> ;
        return (
            <main className='container informations sale'>

                <h1>Vendas</h1>
                {content}
                {saleModal}
                {overlay}

            </main>
        );

    }
}

const mapDispatchToProps = dispatch => ({
    fetchSale: () => {
        dispatch(actions.fetchSale())
    },
    fetchItems: (code) => {
        dispatch(itemActions.fetchItem(code))
    }
})

const mapStateToProps = state => ({
    sales: state.saleReducer.sale,
    items: state.itemReducer.item,
})


export default connect(mapStateToProps, mapDispatchToProps)(ShowSale)

