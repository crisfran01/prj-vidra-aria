import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import EmployForm from './EmployForm';
import Busca from './Busca';



import * as actions from "../../actions/EmploysActions"

// import '../css/Employ.css';



let columns = [
    { title: "Codigo", field: "code" },
    { title: "Nome", field: "nome" },
    { title: "Tel", field: "tel", type: "numeric" },
    { title: "CPF", field: "cpf", type: "numeric" },
    { title: "Endereço", field: "adress"},
    { title: "E-mail", field: "email"}
];


class Employ extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      employPosition: -1,
      content: 'show'
    }
  }

  componentDidMount() {
    this.props.fetchEmploys()
  }

  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextProps.employs !== this.props.employs) {
        let searchEmploys = [];
        let employs = nextProps.employs;
        for(let i = 0; i < Object.keys(employs).length; i ++){
            searchEmploys.push({                
                'nome': employs[i]['nome'],
                'code': employs[i]['cod'],
                'funcao': employs[i]['funcao'],
                'tel': employs[i]['tel'],
                'cpf': employs[i]['cpf'],
                'adress': employs[i]['end']  + ', ' + employs[i]['numb'] + ', ' +employs[i]['bairro'],
                'email': employs[i]['email']
            })
        }
       
        this.setState({
            'searchEmploys' : searchEmploys
        })
    }

  }


  ChangeContentSearch = () =>{
    this.setState({
        'content':'search'
    });
  }


  ChangeContentShow = (event, rowData) =>{
      let position = this.getEmployPositionByCode(rowData['code']);
    this.setState({
        'content':'show',
        'employPosition':  position
    });
  }

  getEmployPositionByCode = (code) => {
    let employs = this.props.employs;
    let position = -1;
    for(let i = 0; i < Object.keys(employs).length; i ++){
        if(employs[i]['cod'] === code){
            position = i;
        }
    }

    return position;

  }

  render() {
    // let content = '';
    let content = this.state.content === 'search' ?  
    <Busca onSelectItem={this.ChangeContentShow} searchColumns={columns} searchItems={this.state.searchEmploys || [] }> </Busca> :
    <EmployForm employPosition={this.state.employPosition} employs={this.props.employs || []} type='see' onSearch={this.ChangeContentSearch} > </EmployForm>;
    return (
      <main className='container informations Employs'>
        
        <h1>{this.props.title}</h1>
        {content}

      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchEmploys: () => {
    dispatch(actions.fetchEmploys())
  }
})

const mapStateToProps = state => ({
  employs: state.employReducer.employs
})


Employ.propTypes = {
  title: PropTypes.string,
}

Employ.defaultProps = {
  title: ''
}


export default connect(mapStateToProps, mapDispatchToProps)(Employ)

