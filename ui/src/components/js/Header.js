import React from 'react';
import Vendas from './Vendas';
import Client from './Client';
import Employ from './Employ';
import Product from './Product';
import Seller from './Seller';
import ShowSale from './ShowSale';
import Home from './Home';
import Users from './Users';
import CondPg from './CondPg';
import Login from './Login';
import ControleDeCaixa from './ControleDeCaixa';
import ContasReceber from './ContasReceber';

import { isAuthenticated, logout } from "../../services/auth";


import { BrowserRouter as Router, Route, Link,Redirect } from "react-router-dom"

import '../css/Header.css';




class Header extends React.Component {

  Search = () =>{
    
  }

  clickLogout = () =>{
    logout();
    window.location ='/login';
  }

  render() {

    let pLogout = ''

    if(isAuthenticated()){
      pLogout= <li class='logout' onClick={this.clickLogout}>Logout</li>
    }

    return (
      <Router>
        <div>
          <header>
            <nav className='header-menu'>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li className='parent'>
                  <span>Cadastros</span>
                  <ul>
                    <li>
                      <Link to="/client/">Clientes</Link>
                    </li>
                    <li>
                      <Link to="/product/">Produtos</Link>
                    </li>
                    <li>
                      <Link to="/workers/">Funcionários</Link>
                    </li>
                    <li>
                      <Link to="/sellers/">Fornecedores</Link>
                    </li>
                  </ul>
                </li>
                <li className='parent'>
                  <span>Vendas</span>
                  <ul>
                    <li>
                      <Link to="/new-sales/">Adicionar</Link>
                    </li>
                    <li>
                      <Link to="/sales/">Buscar</Link>
                    </li>
                  </ul>
                </li>
                <li className='parent'>
                  <span>Financeiro</span>
                  <ul>
                    <li>
                      <Link to="/caixa/">Caixa</Link>
                    </li>
                    <li>
                      <Link to="/contas/">Contas a receber</Link>
                    </li>
                  </ul>
                </li>
                <li className='parent'>
                  <span>Sistema</span>
                  <ul>
                    <li>
                      <Link to="/users/">Usuários</Link>
                    </li>
                    <li>
                      <Link to="/passwords/">Alterar senhas</Link>
                    </li>
                    <li>
                      <Link to="/condicao-pagamento/">Condições de pagamento</Link>
                    </li>
                  </ul>
                </li>
                  {pLogout}
              </ul>
            </nav>
          </header>
          <main>
            <Route path="/" exact component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <Home> </Home>}/>
            <Route path="/client/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <Client title='Clientes' > </Client>} />
            <Route path="/product/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <Product title='Produtos' type='register' > </Product>} />
            <Route path="/sales/"  component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <ShowSale></ShowSale> }/>
            <Route path="/workers/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <Employ title='Funcionários'> </Employ>} />
            <Route path="/sellers/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <Seller title='Fornecedores'> </Seller>} />
            <Route path="/new-sales/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <Vendas> </Vendas>}  />
            <Route path="/users/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />:<Users> </Users>} />
            <Route path="/condicao-pagamento/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <CondPg> </CondPg>} />
            <Route path="/caixa/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <ControleDeCaixa> </ControleDeCaixa>} />
            <Route path="/contas/" component={() => !isAuthenticated() ?  <Redirect to={{ pathname: "/login" }} />: <ContasReceber> </ContasReceber>} />

            <Route path="/login/" component={() => <Login> </Login>} />

          </main>
        </div>
      </Router>
    );

  }

}



export default Header;
