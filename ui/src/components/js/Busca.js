import React from 'react';
import PropTypes from 'prop-types';
import Table from './Table';

import '../css/Busca.css';

class Busca extends React.Component{


  
    render() {

      return (
        <div  className={this.props.className + ' search-table'}>
          <Table  columns={ this.props.searchColumns}
          data={ this.props.searchItems}
          title={this.props.title}
          edit={false}
          onClick={this.props.onSelectItem}       
          pageSize={this.props.pageSize}       
          showSeeIcon={this.props.showSeeIcon}  
          ></Table>
         
        </div>
      );

    }
    
}

Busca.propTypes = {
  searchItems: PropTypes.array,
  onSelectItem: PropTypes.func,
  className: PropTypes.string,
  title: PropTypes.string,
  showSeeIcon: PropTypes.bool,
  pageSize: PropTypes.number,
}

Busca.defaultProps = {
  searchItems: [],
  title: '',
  showSeeIcon: true,
  pageSize: 15
}



export default Busca;
