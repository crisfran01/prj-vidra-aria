import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import Input from './parts/Input';
import TextArea from './parts/TextArea';

import '../css/ClientForm.css';

import * as actions from "../../actions/SellersActions"


import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Save from '@material-ui/icons/Save';
import Edit from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';
import Clear from '@material-ui/icons/Clear';
import Search from '@material-ui/icons/Search';



class SellerForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sellerPosition:this.props.sellerPosition,
      'cod': "",
      'nome_fantasia': "",
      'razao_social': "",
      'servico': "",
      'cnpj': "",
      'ie': "",
      'end': "",
      'num': "",
      'bairro': "",
      'cidade': "",
      'uf': "",
      'cep': "",
      'whats': "",
      'tel': "",
      'email': "",
      'obs': "",
      'type': props.type,
      'editEntity': false,
      'sellers': this.props.sellers
    }
  }

  componentDidMount() {
    if(this.props.sellerPosition !== -1){
      this._fillSeller(this.props.sellerPosition);
    }
  }


  
  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextState.sellerPosition !== this.state.sellerPosition) {
      this._fillSeller(nextState.sellerPosition);
    }

    if (nextProps.sellerPosition !== this.props.sellerPosition) {
      this._fillSeller(nextProps.sellerPosition);
    }

    if (nextProps.createSuccess === true && this.state.type !== 'see'){
      this.setState({
        'type': 'see'
      });
    }

    if (nextProps.updateSuccess === true && this.state.type !== 'see'){
      this.setState({
        'type': 'see'
      });
    }

  }

  _fillSeller = (position) =>{
    let seller = this.props.sellers[position];
    this.setState({
      'cod': seller.cod.toString(),
      'nome_fantasia': seller.nome_fantasia || '',
      'razao_social': seller.razao_social || '',
      'servico': seller.servico || '',
      'cnpj': seller.cnpj || '',
      'ie': seller.ie || '',
      'end': seller.end || '',
      'num': seller.num || '',
      'bairro': seller.bairro || '',
      'cidade': seller.cidade || '',
      'uf': seller.uf || '',
      'cep': seller.cep || '',
      'whats': seller.whats || '',
      'tel': seller.tel || '',
      'email': seller.email || '',
      'obs': seller.obs || '',
      'prev': false,
      'next': true
    })
  }



  PrevSeller = () => {
    let position = this.state.sellerPosition - 1 >= 0 ? this.state.sellerPosition - 1 : this.state.sellerPosition;
    this.setState({
      sellerPosition: position
    });
  }

  NextSeller = (lenght) => {
    let position = this.state.sellerPosition + 1 < lenght ? this.state.sellerPosition + 1 : this.state.sellerPosition;
    this.setState({
      sellerPosition: position,
    });
  }

  Edit = () => {
    this.setState({
      'type': 'register',
      'editEntity': true
    })
  }

  Cancel = () => {

    this.clearMessages();
    this._fillSeller(this.state.sellerPosition);
    this.setState({
      'type': 'see',
      'editEntity': false
    })
    
  }

  Save = () => {
    let data = {};
    let inputs = document.querySelectorAll('.information-data input');
    let selects= document.querySelectorAll('.information-data select');
    let textarea= document.querySelectorAll('.information-data textarea');
    let error = false;
    let cnpj = document.getElementsByName('cnpj')[0];


    this.clearMessages();

    inputs.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })

    selects.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })

    textarea.forEach((input) => {
      data[input.name] = input.value || "";
      if(input.hasAttribute('required') && input.value === "" ){
        error = "true";
        this.createErrorMessage(input.parentElement, "Campo Obrigatorio");
      }
    })

    if(!this.validadePersonDocument(cnpj.value)){
      error = "true";
      this.createErrorMessage(cnpj.parentElement, "CNPJ Inválido");
    }

    if(!error){
      let code = data['cod'];
      data = JSON.stringify(data); 
  
      if(this.state.editEntity){
        this.props.updateSeller(data, code);
      }else{
        this.props.createSellers(data)
      }

    }

  }

  Add = (lenght) =>{

    let cod = this.props.sellers[lenght - 1]['cod'] + 1

    this.setState({
      'cod': cod,
      'nome_fantasia': "",
      'razao_social': "",
      'servico': "",
      'cnpj': "",
      'ie': "",
      'end': "",
      'num': "",
      'bairro': "",
      'cidade': "",
      'uf': "",
      'cep': "",
      'whats': "",
      'tel': "",
      'email': "",
      'obs': "",
      'type': 'register',
      'editEntity': false
    })

  }

validadePersonDocument = (cnpj) => {

  // cnpj = cnpj.replace("-", "").replace("/", "").split('.').join('');
  // cnpj = cnpj.replace(/[^\d]+/g,'');
 
  //   if(cnpj === '') return false;
     
    
  //   // Elimina CNPJs invalidos conhecidos
  //   if (cnpj === "00000000000000" || 
  //       cnpj === "11111111111111" || 
  //       cnpj === "22222222222222" || 
  //       cnpj === "33333333333333" || 
  //       cnpj === "44444444444444" || 
  //       cnpj === "55555555555555" || 
  //       cnpj === "66666666666666" || 
  //       cnpj === "77777777777777" || 
  //       cnpj === "88888888888888" || 
  //       cnpj === "99999999999999")
  //       return false;
        
  //   if (cnpj.length !== 14)
  //       return false;

  //       let tamanho = cnpj.length - 2
  //   let numeros = cnpj.substring(0,tamanho);
  //   let digitos = cnpj.substring(tamanho);
  //   let soma = 0;
  //   let pos = tamanho - 7;
  //   for (let i = tamanho; i >= 1; i--) {
  //     soma += numeros.charAt(tamanho - i) * pos--;
  //     if (pos < 2)
  //           pos = 9;
  //   }
  //   let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
  //   if (resultado !== digitos.charAt(0))
  //       return false;
         
  //   tamanho = tamanho + 1;
  //   numeros = cnpj.substring(0,tamanho);
  //   soma = 0;
  //   pos = tamanho - 7;
  //   for (let i = tamanho; i >= 1; i--) {
  //     soma += numeros.charAt(tamanho - i) * pos--;
  //     if (pos < 2)
  //           pos = 9;
  //   }
  //   resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
  //   if (resultado !== digitos.charAt(1))
  //         return false;
           
    return true;

  
}



  createErrorMessage = (parent, message) => {
    let span = document.createElement('span');
    span.classList.add('error-message');
    span.innerHTML = message;
    parent.appendChild(span);
  }

  clearMessages = () =>{
    let messages= document.querySelectorAll('.error-message');
    messages.forEach((message) => {
     message.remove();
    })
  }



  render() {

    if (Object.keys(this.props.sellers).length > 0 && this.state.sellerPosition === -1) {
      this.setState({
        sellerPosition: 0,
      })
    } 


    let disableNext = this.state.sellerPosition === Object.keys(this.props.sellers).length - 1 ? 'disabled' : '';
    let disablePrev = this.state.sellerPosition === 0 ? 'disabled' : '';

    let controlsOptionsSee = (
      <div className="navegation-controls">
        <button className={'navegation-button prev ' + disablePrev} onClick={this.PrevSeller}> <ChevronLeft></ChevronLeft> </button>
        <button className='edit-button' onClick={this.Edit}> <Edit></Edit> </button>
        <button className='add-button' onClick={() => this.Add(Object.keys(this.props.sellers).length)}> <Add></Add> </button>
        <button className='search-button' onClick={this.props.onSearch}> <Search></Search> </button>
        <button className={'navegation-button next ' + disableNext} onClick={() => this.NextSeller(Object.keys(this.props.sellers).length)}> <ChevronRight></ChevronRight> </button>

      </div>
    )

    let controlsOptionsRegister = (
      <div className="navegation-controls">
        <button className='cancel-button' onClick={this.Cancel}> <Clear></Clear> </button>
        <button className='save-button' onClick={this.Save}> <Save></Save> </button>
      </div>
    );

    let editable = this.state.type === 'register' ? false : true;
    let controls = this.state.type === 'register' ? controlsOptionsRegister : controlsOptionsSee;
    return (
        <div className="information-data">
          <form action='#' method='POST'>
            <Input value={this.state.cod} className='label-input cl3' title='Código' disabled={true} type='number' name='cod' ></Input>
            <Input value={this.state.razao_social} className='label-input cl3' title='Razão Social' disabled={editable} type='text' name='razao_social'  required={true}></Input>
            <Input value={this.state.nome_fantasia} className='label-input cl3' title='Nome Fantasia' disabled={editable} type='text' name='nome_fantasia'  required={true}></Input>
            <Input value={this.state.tel} className='label-input cl3' title='Telefone' mask='(99) 9999-99999' disabled={editable} type='tel' name='tel' ></Input>
            <Input value={this.state.whats} className='label-input cl3' title='Whatsapp' mask='(99) 9999-99999' disabled={editable} type='text' name='whats' ></Input>
            <Input value={this.state.email} className='label-input cl3' title='E-mail' disabled={editable} type='email' name='email' ></Input>
            <Input value={this.state.cnpj} className='label-input cl3' title='CNPJ'  mask='99.999.999/9999-99'  disabled={editable} type='text' name='cnpj'  required={true}></Input>
            <Input value={this.state.ie} className='label-input cl3' title='IE'  mask='999.999.999.999' disabled={editable} type='text' name='ie' ></Input>
            <Input value={this.state.servico} className='label-input cl3' title='Serviço'  disabled={editable} type='text' name='servico' ></Input>
            <Input value={this.state.end} className='label-input cl23' title='Endereço' disabled={editable} type='text' name='end' ></Input>
            <Input value={this.state.num} className='label-input cl3' title='Número' disabled={editable} type='number' name='num' ></Input>
            <Input value={this.state.bairro} className='label-input cl3' title='Bairro' disabled={editable} type='text' name='bairro' ></Input>
            <Input value={this.state.cidade} className='label-input cl3' title='Cidade' disabled={editable} type='text' name='cidade' ></Input>
            <Input value={this.state.cep} className='label-input cl3' title='CEP' mask='99.999-999' disabled={editable} type='text' name='cep' ></Input>
            <TextArea value={this.state.obs}  className='label-textarea' title='Observações' disabled={editable} name='obs'></TextArea>

          </form>

          {controls}

        </div>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchSellers: () => {
    dispatch(actions.fetchSellers())
  },
  createSellers: (data) => {
    dispatch(actions.createSeller(data))
  },
  updateSeller: (data, cod) => {
    dispatch(actions.updateSeller(data, cod))
  }
})

const mapStateToProps = state => ({
  updateSuccess: state.sellersReducer.updateSeller,  
  createSuccess: state.sellersReducer.createSeller,  
})


SellerForm.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  onSearch: PropTypes.func,
  sellerPosition: PropTypes.number,
  sellers: PropTypes.array
}

SellerForm.defaultProps = {
  title: '',
  type: 'see',
  sellerPosition: -1
}


export default connect(mapStateToProps, mapDispatchToProps)(SellerForm)

