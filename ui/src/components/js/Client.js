import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import ClientForm from './ClientForm';
import Busca from './Busca';



import * as actions from "../../actions/ClientsActions"

// import '../css/Client.css';

let columns = [
    { title: "Codigo", field: "code" },
    { title: "Nome", field: "nome" },
    { title: "Tel", field: "tel", type: "numeric" },
    { title: "CPF", field: "cpf", type: "numeric" },
    { title: "Endereço", field: "adress"},
    { title: "E-mail", field: "email"}
];


class Client extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      clientPosition: -1,
      content: 'show'
    }
  }

  componentDidMount() {
    this.props.fetchClients()
  }

  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextProps.clients !== this.props.clients) {
        let searchClients = [];
        let clients = nextProps.clients;
        for(let i = 0; i < Object.keys(clients).length; i ++){
            searchClients.push({                
                'nome': clients[i]['nome'],
                'code': clients[i]['cod'],
                'tel': clients[i]['tel'],
                'cpf': clients[i]['cpf'],
                'adress': clients[i]['end']  + ', ' + clients[i]['num'] + ', ' +clients[i]['bairro'],
                'email': clients[i]['email']
            })
        }
       
        this.setState({
            'searchClients' : searchClients
        })
    }

  }


  ChangeContentSearch = () =>{
    this.setState({
        'content':'search'
    });
  }


  ChangeContentShow = (event, rowData) =>{
      let position = this.getClientPositionByCode(rowData['code']);
    this.setState({
        'content':'show',
        'clientPosition':  position
    });
  }

  getClientPositionByCode = (code) => {
    let clients = this.props.clients;
    let position = -1;
    for(let i = 0; i < Object.keys(clients).length; i ++){
        if(clients[i]['cod'] === code){
            position = i;
        }
    }

    return position;

  }

  render() {

    let content = this.state.content === 'search' ?  
    <Busca onSelectItem={this.ChangeContentShow} searchColumns={columns} searchItems={this.state.searchClients || [] }> </Busca> :
    <ClientForm clientPosition={this.state.clientPosition} clients={this.props.clients || {}} type='see' onSearch={this.ChangeContentSearch} > </ClientForm>;
    return (
      <main className='container informations clients'>
        
        <h1>{this.props.title}</h1>
        {content}

      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchClients: () => {
    dispatch(actions.fetchClients())
  }
})

const mapStateToProps = state => ({
  clients: state.clientsReducer.clients
})


Client.propTypes = {
  title: PropTypes.string,
}

Client.defaultProps = {
  title: ''
}


export default connect(mapStateToProps, mapDispatchToProps)(Client)

