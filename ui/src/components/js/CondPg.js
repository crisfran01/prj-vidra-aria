import React from 'react';
import { connect } from "react-redux"
import Table from './Table';
import Input from './parts/Input';
import TextArea from './parts/TextArea';

import * as condPgActions from "../../actions/CondPgActions"

let condPgColumns = [
  { title: "Codigo", field: "cod" },
  { title: "Descrição", field: "descricao" },
  { title: "Parcelas", field: "parcelas" },
  { title: "Dias para primeiro pagamento", field: "data_first" },
];

class CondP extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      searchCondPg: [],
      cod: "",
      descricao: "",
      update: false,
      parcelas: "",
      data_first:""
    }
   
  }

  componentDidMount() {
    this.props.fetchCondPg();
  }


  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextProps.condPg !== this.props.condPg) {
      let searchCondPg = [];
      let condPg = nextProps.condPg;
      for(let i = 0; i < Object.keys(condPg).length; i ++){
          searchCondPg.push({                
              'descricao': condPg[i]['descricao'],
              'cod': condPg[i]['cod'],
              'parcelas': condPg[i]['parcelas'],
              'data_first': condPg[i]['data_first']
          })
      }
     
      this.setState({
          'searchCondPg' : searchCondPg
      })
    }

  }


   saveCondPg = (event) => {
     event.preventDefault();

     let error = false;
    
    if(this.state.descricao === ""){
      error = 'Coloque uma descrição.';
    }

    if(!error){
     
      let data = {
        'cod': this.state.code,
        'descricao': this.state.descricao,
        'parcelas': this.state.parcelas,
        'data_first': this.state.data_first,
      };
  
      this.props.createCondPg(data);

   
      this.setState({
        code: "",
        descricao: "",
        parcelas: "",
        data_first:""
      })

    }else{
      this.setState({ errorMessage : error })
    }

   }



   changeInput = (name, value) => {
      this.setState({
        [name] : value,
        inputUpdate: true,
      })   
   }

   update = (newData, oldData) =>{

    let data = {
      'descricao': newData.descricao,
      'parcelas': newData.parcelas,
      'data_first': newData.data_first,
    };

    this.props.updateCondPg(data, newData.cod);

  }

   delete = (oldData) =>{

    this.props.deleteCondPg(oldData.cod);

   }
  

  render() {

    console.log(this.state)
    return (
      <main className='user sell container'>
        <h1> Condições de pagamento</h1>
        <div className='sell-cli'>
          <h2>Cadastrar Condição de pagamento:</h2>
          <form className='client-form'>
            <TextArea onChange={this.changeInput} className='label-input cl1' value={this.state['descricao'] || ''} type='text' required name='descricao' placeholder='Descrição'></TextArea >
            <Input onChange={this.changeInput} value={this.state.parcelas} className='label-input cl40' title='Número de parcelas'  type='number' name='parcelas'  required={true}></Input>
            <Input onChange={this.changeInput} value={this.state.data_first} className='label-input cl40' title='Quantos dias para o primeiro pagamento'  type='number' name='data_first'  required={true}></Input>
            <button onClick={this.saveCondPg} className='sell-item-insert label-input cl20' > Inserir</button >
            <p className='item-error-message'>{this.state.errorMessage}</p>
          </form>
        </div>
        <div className='items-contaner'>
          <Table columns={condPgColumns}
            data={this.state.searchCondPg}
            title=''
            edit={true}
            exportButton={true}
            pageSize={8}
            showSeeIcon={false}
            changeUpdateData={this.update}
            deleteData={this.delete}
          ></Table>
        </div>

      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchCondPg: () => {
    dispatch(condPgActions.fetchCondPgs())
  },
  createCondPg: (data) => {
    dispatch(condPgActions.createCondPg(data))
  },
  updateCondPg: (data, code) => {
    dispatch(condPgActions.updateCondPg(data, code))
  },
  deleteCondPg: (code) => {
    dispatch(condPgActions.deleteCondPg(code))
  }
})

const mapStateToProps = state => ({
  condPg: state.condPgReducer.condPgs,
  createCondPg: state.condPgReducer.createCondPg,
})




export default connect(mapStateToProps, mapDispatchToProps)(CondP)
