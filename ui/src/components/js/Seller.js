import React from 'react';
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import SellerForm from './SellerForm';
import Busca from './Busca';



import * as actions from "../../actions/SellersActions"

// import '../css/Seller.css';

let columns = [
    { title: "Codigo", field: "code" },
    { title: "Nome Fantasia", field: "nome_fantasia" },
    { title: "Razão Social", field: "razao_social" },
    { title: "Tel", field: "tel", type: "numeric" },
    { title: "CNPJ", field: "cnpj", type: "numeric" },
    { title: "Endereço", field: "adress"},
    { title: "E-mail", field: "email"}
];


class Seller extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sellerPosition: -1,
      content: 'show'
    }
  }

  componentDidMount() {
    this.props.fetchSellers()
  }

  UNSAFE_componentWillUpdate = (nextProps, nextState) => {

    if (nextProps.sellers !== this.props.sellers) {
        let searchSellers = [];
        let sellers = nextProps.sellers;
        for(let i = 0; i < Object.keys(sellers).length; i ++){
            searchSellers.push({                
                'nome_fantasia': sellers[i]['nome_fantasia'],
                'razao_social': sellers[i]['razao_social'],
                'code': sellers[i]['cod'],
                'tel': sellers[i]['tel'],
                'cnpj': sellers[i]['cnpj'],
                'adress': sellers[i]['end']  + ', ' + sellers[i]['numb'] + ', ' +sellers[i]['bairro'],
                'email': sellers[i]['email']
            })
        }
       
        this.setState({
            'searchSellers' : searchSellers
        })
    }

  }


  ChangeContentSearch = () =>{
    this.setState({
        'content':'search'
    });
  }


  ChangeContentShow = (event, rowData) =>{
      let position = this.getSellerPositionByCode(rowData['code']);
    this.setState({
        'content':'show',
        'sellerPosition':  position
    });
  }

  getSellerPositionByCode = (code) => {
    let sellers = this.props.sellers;
    let position = -1;
    for(let i = 0; i < Object.keys(sellers).length; i ++){
        if(sellers[i]['cod'] === code){
            position = i;
        }
    }

    return position;

  }

  render() {

    let content = this.state.content === 'search' ?  
    <Busca onSelectItem={this.ChangeContentShow} searchColumns={columns} searchItems={this.state.searchSellers || [] }> </Busca> :
    <SellerForm sellerPosition={this.state.sellerPosition} sellers={this.props.sellers || {}} type='see' onSearch={this.ChangeContentSearch} > </SellerForm>;
    return (
      <main className='container informations sellers'>
        
        <h1>{this.props.title}</h1>
        {content}

      </main>
    );

  }

}

const mapDispatchToProps = dispatch => ({
  fetchSellers: () => {
    dispatch(actions.fetchSellers())
  }
})

const mapStateToProps = state => ({
  sellers: state.sellersReducer.sellers
})


Seller.propTypes = {
  title: PropTypes.string,
}

Seller.defaultProps = {
  title: ''
}


export default connect(mapStateToProps, mapDispatchToProps)(Seller)

