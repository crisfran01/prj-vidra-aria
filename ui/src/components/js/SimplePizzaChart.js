import React, { Component } from 'react';
import {
  PieChart, Pie, Sector, Cell,
} from 'recharts';

const data = [
  { name: 'Vidro temperado 8mm', value: 400 },
  { name: 'Persianas', value: 300 },
  { name: 'Canelado', value: 278 },
  { name: 'Vidro incolor 3mm', value: 300 },
];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const RADIAN = Math.PI / 180;
 const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index
  }) => {
      const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
      const x = cx + radius * Math.cos(-midAngle * RADIAN);
      const y = cy + radius * Math.sin(-midAngle * RADIAN);

return (
<text
  x={x}
  y={y}
  fill="white"
  textAnchor={x > cx ? "start" : "end"}
  dominantBaseline="central"
>
  {/* {`${(percent * 100).toFixed(0)}%`} */}
  {data[index].name}
</text>
);
};

class SimplePizzaChart extends Component {
  
    render() {
        return (
          <PieChart width={450} height={200}>

                <Pie
                    data={data}
                    cx={200}
                    cy={100}
                    label={({
                      cx,
                      cy,
                      midAngle,
                      innerRadius,
                      outerRadius,
                      value,
                      index
                    }) => {
                      console.log("handling label?");
                      const RADIAN = Math.PI / 180;
                      // eslint-disable-next-line
                      const radius = 25 + innerRadius + (outerRadius - innerRadius);
                      // eslint-disable-next-line
                      const x = cx + radius * Math.cos(-midAngle * RADIAN);
                      // eslint-disable-next-line
                      const y = cy + radius * Math.sin(-midAngle * RADIAN);
            
                      return (
                        <text
                          x={x}
                          y={y}
                          fill="#8884d8"
                          textAnchor={x > cx ? "start" : "end"}
                          dominantBaseline="central"
                        >
                          {data[index].name} ({value})
                        </text>
                      );
                    }}
                    outerRadius={80}
                    fill="#8884d8"
                    dataKey="value"
                  >
                    {
                      data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                    }
                  </Pie>
        </PieChart>
        );
      }
}

export default SimplePizzaChart;

