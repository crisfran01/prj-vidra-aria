<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Sales;
use App\Entity\FullSales;
use App\Form\SalesType;


/**
 * Sales controller.
 * @Route("/api", name="api_")
 */
class SalesController extends FOSRestController
{
  /**
   * Lists all Sales.
   * @Rest\Get("/sales")
   *
   * @return Response
   */
  public function getSalesAction()
  {
    $repository = $this->getDoctrine()->getRepository(FullSales::class);
    $sales = $repository->findBy(array(), array("cod"  => 'ASC'));
    return $this->handleView($this->view($sales));
    
  }
  /**
   * Create Sales.
   * @Rest\Post("/sale")
   *
   * @return Response
   */
  public function postSalesAction(Request $request)
  {
    $sale = new Sales();
    $form = $this->createForm(SalesType::class, $sale);
    $data = json_decode($request->getContent(), true);
    $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['data'] );
    $data['data'] = $phpdate->format('Y-m-d H:i:s');
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($sale);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update Client.
   * @Rest\Put("/sale/{cod}")
   *
   * @return Response
   */
  public function putSalesAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(Sales::class);
    $sale = $repository->find($cod);
    if(!$sale){
      return $this->handleView($this->view(['status' => 'Cliente não existe'], 404));

    }
    $form = $this->createForm(SalesType::class, $sale);
    $data = json_decode($request->getContent(), true);
    $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['data'] );
    $data['data'] = $phpdate->format('Y-m-d H:i:s');
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($sale);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Client.
   * @Rest\Delete("/sale/{cod}")
   *
   * @return Response
   */
  public function deleteSalesAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Sales::class);
    $sale = $repository->find($cod);
    if(!$sale){
      return $this->handleView($this->view(['status' => 'Produto não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($sale);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }


  /**
   * Lists all Sales.
   * @Rest\Get("/sale/new-code")
   *
   * @return Response
   */
  public function getSaleNewAction()
  {
    $repository = $this->getDoctrine()->getRepository(FullSales::class);
    $sale = $repository->findOneBy([], ['cod' => 'desc']);
    return $this->handleView($this->view($sale->getCod() + 1));
    
  }

}


