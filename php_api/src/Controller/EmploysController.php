<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Employs;
use App\Form\EmploysType;
/**
 * Employs controller.
 * @Route("/api", name="api_")
 */
class EmploysController extends FOSRestController
{
  /**
   * Lists all Employs.
   * @Rest\Get("/employs")
   *
   * @return Response
   */
  public function getEmploysAction()
  {
    $repository = $this->getDoctrine()->getRepository(Employs::class);
    $employs = $repository->findBy(array(), array("cod"  => 'ASC'));
    return $this->handleView($this->view($employs));
    
  }
  /**
   * Create Employs.
   * @Rest\Post("/employ")
   *
   * @return Response
   */
  public function postEmploysAction(Request $request)
  {
    $employ = new Employs();
    $form = $this->createForm(EmploysType::class, $employ);
    $data = json_decode($request->getContent(), true);
    if(array_key_exists('nascimento',$data ) && $data['nascimento'] != ""){
      $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['nascimento'] );
      $data['nascimento'] = $phpdate->format('Y-m-d H:i:s');
    }
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($employ);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update Employ.
   * @Rest\Put("/employ/{cod}")
   *
   * @return Response
   */
  public function putEmploysAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(Employs::class);
    $employ = $repository->find($cod);
    if(!$employ){
      return $this->handleView($this->view(['status' => 'Empregado não existe'], 404));

    }
    $form = $this->createForm(EmploysType::class, $employ);
    $data = json_decode($request->getContent(), true);
    if(array_key_exists('nascimento',$data ) && $data['nascimento'] != ""){
      $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['nascimento'] );
      $data['nascimento'] = $phpdate->format('Y-m-d H:i:s');
    }
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($employ);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Employ.
   * @Rest\Delete("/employ/{cod}")
   *
   * @return Response
   */
  public function deleteEmploysAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Employs::class);
    $employ = $repository->find($cod);
    if(!$employ){
      return $this->handleView($this->view(['status' => 'Empregado não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($employ);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
}


