<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Users;
use App\Entity\FullUsers;
use App\Form\UsersType;


/**
 * Users controller.
 * @Route("/api", name="api_")
 */
class UsersController extends FOSRestController
{
  /**
   * Lists all Users.
   * @Rest\Get("/users")
   *
   * @return Response
   */
  public function getUsersAction()
  {
    $repository = $this->getDoctrine()->getRepository(FullUsers::class);
    $users = $repository->findBy(array(), array("cod"  => 'ASC'));
    return $this->handleView($this->view($users));
    
  }
  /**
   * Create Users.
   * @Rest\Post("/user")
   *
   * @return Response
   */
  public function postUsersAction(Request $request)
  {
    $user = new Users();
    $form = $this->createForm(UsersType::class, $user);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update User.
   * @Rest\Put("/user/{cod}")
   *
   * @return Response
   */
  public function putUsersAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(Users::class);
    $user = $repository->find($cod);
    if(!$user){
      return $this->handleView($this->view(['status' => 'User não existe'], 404));

    }
    $form = $this->createForm(UsersType::class, $user);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete User.
   * @Rest\Delete("/user/{cod}")
   *
   * @return Response
   */
  public function deleteUsersAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Users::class);
    $user = $repository->find($cod);
    if(!$user){
      return $this->handleView($this->view(['status' => 'User não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($user);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
  
  /**
   * Lists sale Users.
   * @Rest\Get("/sale/{cod}/users")
   *
   * @return Response
   */
  public function getSaleUsersAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Users::class);
    $users = $repository->findBy(array('cod_vend' => $cod));
    return $this->handleView($this->view($users));
    
  }
  
  
  /**
   * Create Users.
   * @Rest\Post("/user/login")
   *
   * @return Response
   */
  public function postLoginAction(Request $request)
  {
    echo 'a';
    $data = json_decode($request->getContent(), true);
    $repository = $this->getDoctrine()->getRepository(Users::class);
    print_r($request);
    $user = $repository->findBy(array('nome' => $data['nome']));
    if(!$user){
        return $this->handleView($this->view(['status' => 'Usuário não existe'], 404));
    }

    if($user->getSenha() != $data['senha']){
      //return $this->handleView($this->view(['status' => 'Senha Incorreta'], 404));
    }

    return $this->handleView($this->view(['id' => 'tre'], Response::HTTP_OK));
  }

}