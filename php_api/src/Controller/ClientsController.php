<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Clients;
use App\Form\ClientsType;

/**
 * Clients controller.
 * @Route("/api", name="api_")
 */
class ClientsController extends FOSRestController
{
  /**
   * Lists all Clients.
   * @Rest\Get("/clients")
   * @Rest\Options("/clients")
   *
   * @return Response
   */
  public function getClientsAction()
  {
    $repository = $this->getDoctrine()->getRepository(Clients::class);
    $clients = $repository->findBy(array(), array("cod"  => 'ASC'));;
    return $this->handleView($this->view($clients));
    
  }
  /**
   * Create Clients.
   * @Rest\Post("/client")
   *
   * @return Response
   */
  public function postClientsAction(Request $request)
  {
    $client = new Clients();
    $form = $this->createForm(ClientsType::class, $client);
    $data = json_decode($request->getContent(), true);
    if(array_key_exists('nascimento',$data ) && $data['nascimento'] != ""){
      $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['nascimento'] );
      $data['nascimento'] = $phpdate->format('Y-m-d H:i:s');
    }
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($client);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update Client.
   * @Rest\Put("/client/{cod}")
   *
   * @return Response
   */
  public function putClientsAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(Clients::class);
    $client = $repository->find($cod);
    if(!$client){
      return $this->handleView($this->view(['status' => 'Cliente não existe'], 404));

    }
    $form = $this->createForm(ClientsType::class, $client);
    $data = json_decode($request->getContent(), true);
    if(array_key_exists('nascimento',$data ) && $data['nascimento'] != ""){
      $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['nascimento'] );
      $data['nascimento'] = $phpdate->format('Y-m-d H:i:s');
    }
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($client);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Client.
   * @Rest\Delete("/client/{cod}")
   *
   * @return Response
   */
  public function deleteClientsAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Clients::class);
    $client = $repository->find($cod);
    if(!$client){
      return $this->handleView($this->view(['status' => 'Cliente não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($client);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
}


