<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\CondPgs;
use App\Form\CondPgsType;


/**
 * CondPgs controller.
 * @Route("/api", name="api_")
 */
class CondPgsController extends FOSRestController
{
  /**
   * Lists all CondPgs.
   * @Rest\Get("/condPgs")
   *
   * @return Response
   */
  public function getCondPgsAction()
  {
    $repository = $this->getDoctrine()->getRepository(CondPgs::class);
    $condPgs = $repository->findBy(array(), array("cod"  => 'ASC'));
    return $this->handleView($this->view($condPgs));
    
  }
  /**
   * Create CondPgs.
   * @Rest\Post("/condPg")
   *
   * @return Response
   */
  public function postCondPgsAction(Request $request)
  {
    $condPg = new CondPgs();
    $form = $this->createForm(CondPgsType::class, $condPg);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($condPg);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update CondPg.
   * @Rest\Put("/condPg/{cod}")
   *
   * @return Response
   */
  public function putCondPgsAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(CondPgs::class);
    $condPg = $repository->find($cod);
    if(!$condPg){
      return $this->handleView($this->view(['status' => 'Condição não existe'], 404));

    }
    $form = $this->createForm(CondPgsType::class, $condPg);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($condPg);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Client.
   * @Rest\Delete("/condPg/{cod}")
   *
   * @return Response
   */
  public function deleteCondPgsAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(CondPgs::class);
    $condPg = $repository->find($cod);
    if(!$condPg){
      return $this->handleView($this->view(['status' => 'Condição não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($condPg);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
}


