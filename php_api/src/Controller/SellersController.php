<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Sellers;
use App\Form\SellersType;
/**
 * Sellers controller.
 * @Route("/api", name="api_")
 */
class SellersController extends FOSRestController
{
  /**
   * Lists all Sellers.
   * @Rest\Get("/sellers")
   *
   * @return Response
   */
  public function getSellersAction()
  {
    $repository = $this->getDoctrine()->getRepository(Sellers::class);
    $sellers = $repository->findBy(array(), array("cod"  => 'ASC'));
    return $this->handleView($this->view($sellers));    
  }
  /**
   * Create Sellers.
   * @Rest\Post("/seller")
   *
   * @return Response
   */
  public function postSellersAction(Request $request)
  {
    $seller = new Sellers();
    $form = $this->createForm(SellersType::class, $seller);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($seller);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update Seller.
   * @Rest\Put("/seller/{cod}")
   *
   * @return Response
   */
  public function putSellersAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(Sellers::class);
    $seller = $repository->find($cod);
    if(!$seller){
      return $this->handleView($this->view(['status' => 'Sellere não existe'], 404));

    }
    $form = $this->createForm(SellersType::class, $seller);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($seller);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Seller.
   * @Rest\Delete("/seller/{cod}")
   *
   * @return Response
   */
  public function deleteSellersAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Sellers::class);
    $seller = $repository->find($cod);
    if(!$seller){
      return $this->handleView($this->view(['status' => 'Sellere não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($seller);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
}


