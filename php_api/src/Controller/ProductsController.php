<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Products;
use App\Form\ProductsType;


/**
 * Products controller.
 * @Route("/api", name="api_")
 */
class ProductsController extends FOSRestController
{
  /**
   * Lists all Products.
   * @Rest\Get("/products")
   *
   * @return Response
   */
  public function getProductsAction()
  {
    $repository = $this->getDoctrine()->getRepository(Products::class);
    $products = $repository->findBy(array(), array("cod"  => 'ASC'));
    return $this->handleView($this->view($products));
    
  }
  /**
   * Create Products.
   * @Rest\Post("/product")
   *
   * @return Response
   */
  public function postProductsAction(Request $request)
  {
    $product = new Products();
    $form = $this->createForm(ProductsType::class, $product);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($product);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update Client.
   * @Rest\Put("/product/{cod}")
   *
   * @return Response
   */
  public function putProductsAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(Products::class);
    $product = $repository->find($cod);
    if(!$product){
      return $this->handleView($this->view(['status' => 'Cliente não existe'], 404));

    }
    $form = $this->createForm(ProductsType::class, $product);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($product);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Client.
   * @Rest\Delete("/product/{cod}")
   *
   * @return Response
   */
  public function deleteProductsAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Products::class);
    $product = $repository->find($cod);
    if(!$product){
      return $this->handleView($this->view(['status' => 'Produto não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($product);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
}


