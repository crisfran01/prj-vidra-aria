<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Items;
use App\Entity\FullItems;
use App\Form\ItemsType;


/**
 * Items controller.
 * @Route("/api", name="api_")
 */
class ItemsController extends FOSRestController
{
  /**
   * Lists all Items.
   * @Rest\Get("/items")
   *
   * @return Response
   */
  public function getItemsAction()
  {
    $repository = $this->getDoctrine()->getRepository(FullItems::class);
    $items = $repository->findBy(array(), array("cod_prod"  => 'ASC'));
    return $this->handleView($this->view($items));
    
  }
  /**
   * Create Items.
   * @Rest\Post("/item")
   *
   * @return Response
   */
  public function postItemsAction(Request $request)
  {
    $item = new Items();
    $form = $this->createForm(ItemsType::class, $item);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($item);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update Item.
   * @Rest\Put("/item/{cod}")
   *
   * @return Response
   */
  public function putItemsAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(Items::class);
    $item = $repository->find($cod);
    if(!$item){
      return $this->handleView($this->view(['status' => 'Item não existe'], 404));

    }
    $form = $this->createForm(ItemsType::class, $item);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($item);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Item.
   * @Rest\Delete("/item/{cod}")
   *
   * @return Response
   */
  public function deleteItemsAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(Items::class);
    $item = $repository->find($cod);
    if(!$item){
      return $this->handleView($this->view(['status' => 'Item não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($item);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
  
  /**
   * Lists sale Items.
   * @Rest\Get("/sale/{cod}/items")
   *
   * @return Response
   */
  public function getSaleItemsAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(FullItems::class);
    $items = $repository->findBy(array('cod_vend' => $cod), array("cod_prod"  => 'ASC'));
    return $this->handleView($this->view($items));
    
  }
}