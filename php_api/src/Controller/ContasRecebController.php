<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\ContasReceb;
use App\Form\ContasRecebType;


/**
 * ContasReceb controller.
 * @Route("/api", name="api_")
 */
class ContasRecebController extends FOSRestController
{
  /**
   * Lists all ContasReceb.
   * @Rest\Get("/contas")
   *
   * @return Response
   */
  public function gtCContasRecebAction()
  {
    $repository = $this->getDoctrine()->getRepository(ContasReceb::class);
    $contasReceb = $repository->findall();
    return $this->handleView($this->view($contasReceb));
    
  }
  /**
   * Create ContasReceb.
   * @Rest\Post("/conta")
   *
   * @return Response
   */
  public function pstCContasRecebAction(Request $request)
  {
    $conta = new ContasReceb();
    $form = $this->createForm(ContasRecebType::class, $conta);
    $data = json_decode($request->getContent(), true);
    $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['data_criacao'] );
    $data['data_criacao'] = $phpdate->format('Y-m-d H:i:s');
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($conta);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

   /**
   * Update CondPg.
   * @Rest\Put("/conta/{cod}")
   *
   * @return Response
   */
  public function ptCContasRecebAction($cod,Request $request )
  {
    $repository = $this->getDoctrine()->getRepository(ContasReceb::class);
    $conta = $repository->find($cod);
    if(!$conta){
      return $this->handleView($this->view(['status' => 'Condição não existe'], 404));
    }
    $form = $this->createForm(ContasRecebType::class, $conta);
    $data = json_decode($request->getContent(), true);
    $phpdate = \DateTime::createFromFormat('!d/m/Y',  $data['data_criacao'] );
    $data['data_criacao'] = $phpdate->format('Y-m-d H:i:s');
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($conta);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors(), 404));
  }

    /**
   * Delete Client.
   * @Rest\Delete("/conta/{cod}")
   *
   * @return Response
   */
  public function dleteCContasRecebAction($cod)
  {
    $repository = $this->getDoctrine()->getRepository(ContasReceb::class);
    $conta = $repository->find($cod);
    if(!$conta){
      return $this->handleView($this->view(['status' => 'Condição não existe'], 404));

    }
   
    $em = $this->getDoctrine()->getManager();
    $em->remove($conta);
    $em->flush();
    return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
    
  }
}


