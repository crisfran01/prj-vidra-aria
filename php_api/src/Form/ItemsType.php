<?php
namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Items;

class ItemsType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('cod')
      ->add('cod_vend')
      ->add('cod_prod')
      ->add('altura')
      ->add('largura')
      ->add('metragem')
      ->add('qtde')
      ->add('preco')
      ->add('save', SubmitType::class)
    ;
  }
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Items::class,
      'csrf_protection' => false
    ));
  }
}