<?php
namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Clients;

class ClientsType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('cod')
      ->add('nome')
      ->add('apelido')
      ->add('nascimento')
      ->add('cpf')
      ->add('rg')
      ->add('end')
      ->add('num')
      ->add('bairro')
      ->add('cidade')
      ->add('uf')
      ->add('cep')
      ->add('whats')
      ->add('tel')
      ->add('email')
      ->add('obs')
      ->add('person')
      ->add('save', SubmitType::class)
    ;
  }
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Clients::class,
      'csrf_protection' => false
    ));
  }
}