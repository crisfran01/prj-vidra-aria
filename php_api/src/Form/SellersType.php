<?php
namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Sellers;

class SellersType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('cod')
      ->add('nome_fantasia')
      ->add('razao_social')
      ->add('servico')
      ->add('cnpj')
      ->add('ie')
      ->add('end')
      ->add('num')
      ->add('bairro')
      ->add('cidade')
      ->add('uf')
      ->add('cep')
      ->add('whats')
      ->add('tel')
      ->add('email')
      ->add('obs')
      ->add('save', SubmitType::class)
    ;
  }
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Sellers::class,
      'csrf_protection' => false
    ));
  }
}