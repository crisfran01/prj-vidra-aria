<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="produtos")
 */
class Products {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;

  /**
   * @ORM\Column(type="string", length=200)
   * @Assert\NotBlank()
   *
   */
  private $nome;

  /**
   * @ORM\Column(type="text")
   * @Assert\NotBlank()
   */
  private $descricao;

  /**
   * @ORM\Column(type="float")
   */
  private $pr_custo;


  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   */
  private $pr_venda;

  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   */
  private $qtde;

  /**
   * @ORM\Column(type="text")
   * @Assert\NotBlank()
   */
  private $unid_med;



  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of nome
   */ 
  public function getNome()
  {
    return $this->nome;
  }

  /**
   * Set the value of nome
   *
   * @return  self
   */ 
  public function setNome($nome)
  {
    $this->nome = $nome;

    return $this;
  }

  /**
   * Get the value of descricao
   */ 
  public function getDescricao()
  {
    return $this->descricao;
  }

  /**
   * Set the value of descricao
   *
   * @return  self
   */ 
  public function setDescricao($descricao)
  {
    $this->descricao = $descricao;

    return $this;
  }

  /**
   * Get the value of pr_custo
   */ 
  public function getPrCusto()
  {
    return $this->pr_custo;
  }

  /**
   * Set the value of pr_custo
   *
   * @return  self
   */ 
  public function setPrCusto($pr_custo)
  {
    $this->pr_custo = $pr_custo;

    return $this;
  }

  /**
   * Get the value of pr_venda
   */ 
  public function getPrVenda()
  {
    return $this->pr_venda;
  }

  /**
   * Set the value of pr_venda
   *
   * @return  self
   */ 
  public function setPrVenda($pr_venda)
  {
    $this->pr_venda = $pr_venda;

    return $this;
  }

  /**
   * Get the value of qtde
   */ 
  public function getQtde()
  {
    return $this->qtde;
  }

  /**
   * Set the value of qtde
   *
   * @return  self
   */ 
  public function setQtde($qtde)
  {
    $this->qtde = $qtde;

    return $this;
  }

  /**
   * Get the value of unid_med
   */ 
  public function getUnidMed()
  {
    return $this->unid_med;
  }

  /**
   * Set the value of unid_med
   *
   * @return  self
   */ 
  public function setUnidMed($unid_med)
  {
    $this->unid_med = $unid_med;

    return $this;
  }
}
  