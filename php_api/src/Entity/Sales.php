<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="vendas")
 */
class Sales {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;

  /**
   * @ORM\Column(type="string", length=200)
   *
  */
  private $obs;

   /**
   * @ORM\Column(type="string")
   * @Assert\NotBlank()
   *
  */
  private $data;

   /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   *
  */
  private $cod_fun;

   /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   *
  */
  private $cod_cli;

  /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   *
  */
  private $cod_pg;


  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   *
  */
  private $valor;




  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of obs
   */ 
  public function getObs()
  {
    return $this->obs;
  }

  /**
   * Set the value of obs
   *
   * @return  self
   */ 
  public function setObs($obs)
  {
    $this->obs = $obs;

    return $this;
  }

  /**
   * Get the value of data
   */ 
  public function getData()
  {
    return $this->data;
  }

  /**
   * Set the value of data
   *
   * @return  self
   */ 
  public function setData($data)
  {
    $this->data = $data;

    return $this;
  }

  /**
   * Get the value of valor
   */ 
  public function getValor()
  {
    return $this->valor;
  }

  /**
   * Set the value of valor
   *
   * @return  self
   */ 
  public function setValor($valor)
  {
    $this->valor = $valor;

    return $this;
  }

  /**
   * Get the value of cod_fun
   */ 
  public function getCodFun()
  {
    return $this->cod_fun;
  }

  /**
   * Set the value of cod_fun
   *
   * @return  self
   */ 
  public function setCodFun($cod_fun)
  {
    $this->cod_fun = $cod_fun;

    return $this;
  }

  /**
   * Get the value of cod_cli
   */ 
  public function getCodCli()
  {
    return $this->cod_cli;
  }

  /**
   * Set the value of cod_cli
   *
   * @return  self
   */ 
  public function setCodCli($cod_cli)
  {
    $this->cod_cli = $cod_cli;

    return $this;
  }

  /**
   * Get the value of cod_pg
   */ 
  public function getCodPg()
  {
    return $this->cod_pg;
  }

  /**
   * Set the value of cod_pg
   *
   * @return  self
   */ 
  public function setCodPg($cod_pg)
  {
    $this->cod_pg = $cod_pg;

    return $this;
  }
}