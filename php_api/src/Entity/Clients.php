<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity
 * @ORM\Table(name="clientes")
 * @UniqueEntity(
 *     fields={"cpf"},
 *     message="CPF já cadastrado"
 * )
 */
class Clients {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank()
   *
   */
  private $nome;

  /**
   * @ORM\Column(type="text")
   */
  private $apelido;

   /**
   * @ORM\Column(type="text")
   */
  private $nascimento;


   /**
   * @ORM\Column(type="text", length=255, unique=true)
   * @Assert\NotBlank()
   */
  private $cpf;


   /**
   * @ORM\Column(type="text")
   */
  private $rg;


   /**
   * @ORM\Column(type="text")
   */
  private $end;

    /**
   * @ORM\Column(type="integer")
   */
  private $num;

    /**
   * @ORM\Column(type="text")
   */
  private $bairro;

    /**
   * @ORM\Column(type="text")
   */
  private $cidade;

    /**
   * @ORM\Column(type="text")
   */
  private $uf;

    /**
   * @ORM\Column(type="text")
   */
  private $cep;

    /**
   * @ORM\Column(type="text")
   */
  private $whats;

  /**
   * @ORM\Column(type="text")
   */
  private $tel;

  /**
   * @ORM\Column(type="text")
   */
  private $email;

  /**
   * @ORM\Column(type="text")
   */
  private $obs;

  /**
   * @ORM\Column(type="text")
   */
  private $person;

  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of apelido
   */ 
  public function getApelido()
  {
    return $this->apelido;
  }

  /**
   * Set the value of apelido
   *
   * @return  self
   */ 
  public function setApelido($apelido)
  {
    $this->apelido = $apelido;

    return $this;
  }

  /**
   * Get the value of cpf
   */ 
  public function getCpf()
  {
    return $this->cpf;
  }

  /**
   * Set the value of cpf
   *
   * @return  self
   */ 
  public function setCpf($cpf)
  {
    $this->cpf = $cpf;

    return $this;
  }

  /**
   * Get the value of rg
   */ 
  public function getRg()
  {
    return $this->rg;
  }

  /**
   * Set the value of rg
   *
   * @return  self
   */ 
  public function setRg($rg)
  {
    $this->rg = $rg;

    return $this;
  }

  /**
   * Get the value of end
   */ 
  public function getEnd()
  {
    return $this->end;
  }

  /**
   * Set the value of end
   *
   * @return  self
   */ 
  public function setEnd($end)
  {
    $this->end = $end;

    return $this;
  }

  /**
   * Get the value of num
   */ 
  public function getNum()
  {
    return $this->num;
  }

  /**
   * Set the value of num
   *
   * @return  self
   */ 
  public function setNum($num)
  {
    $this->num = $num;

    return $this;
  }

  /**
   * Get the value of bairro
   */ 
  public function getBairro()
  {
    return $this->bairro;
  }

  /**
   * Set the value of bairro
   *
   * @return  self
   */ 
  public function setBairro($bairro)
  {
    $this->bairro = $bairro;

    return $this;
  }

  /**
   * Get the value of cidade
   */ 
  public function getCidade()
  {
    return $this->cidade;
  }

  /**
   * Set the value of cidade
   *
   * @return  self
   */ 
  public function setCidade($cidade)
  {
    $this->cidade = $cidade;

    return $this;
  }

  /**
   * Get the value of whats
   */ 
  public function getWhats()
  {
    return $this->whats;
  }

  /**
   * Set the value of whats
   *
   * @return  self
   */ 
  public function setWhats($whats)
  {
    $this->whats = $whats;

    return $this;
  }

  /**
   * Get the value of tel
   */ 
  public function getTel()
  {
    return $this->tel;
  }

  /**
   * Set the value of tel
   *
   * @return  self
   */ 
  public function setTel($tel)
  {
    $this->tel = $tel;

    return $this;
  }

  /**
   * Get the value of email
   */ 
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set the value of email
   *
   * @return  self
   */ 
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
   * Get the value of obs
   */ 
  public function getObs()
  {
    return $this->obs;
  }

  /**
   * Set the value of obs
   *
   * @return  self
   */ 
  public function setObs($obs)
  {
    $this->obs = $obs;

    return $this;
  }

  /**
   * Get the value of person
   */ 
  public function getPerson()
  {
    return $this->person;
  }

  /**
   * Set the value of person
   *
   * @return  self
   */ 
  public function setPerson($person)
  {
    $this->person = $person;

    return $this;
  }

  /**
   * Get the value of nome
   */ 
  public function getNome()
  {
    return $this->nome;
  }

  /**
   * Set the value of nome
   *
   * @return  self
   */ 
  public function setNome($nome)
  {
    $this->nome = $nome;

    return $this;
  }

  /**
   * Get the value of nascimento
   */ 
  public function getNascimento()
  {
    return $this->nascimento;
  }

  /**
   * Set the value of nascimento
   *
   * @return  self
   */ 
  public function setNascimento($nascimento)
  {
    $this->nascimento = $nascimento;

    return $this;
  }

  /**
   * Get the value of uf
   */ 
  public function getUf()
  {
    return $this->uf;
  }

  /**
   * Set the value of uf
   *
   * @return  self
   */ 
  public function setUf($uf)
  {
    $this->uf = $uf;

    return $this;
  }

  /**
   * Get the value of cep
   */ 
  public function getCep()
  {
    return $this->cep;
  }

  /**
   * Set the value of cep
   *
   * @return  self
   */ 
  public function setCep($cep)
  {
    $this->cep = $cep;

    return $this;
  }
}
  