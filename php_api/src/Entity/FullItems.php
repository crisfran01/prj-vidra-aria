<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="full_item")
 */
class FullItems {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;


  /**
   * @ORM\Column(type="string", length=200)
   *
  */
  private $nome;


   /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   *
  */
  private $cod_vend;

   /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   *
  */
  private $cod_prod;


  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   *
  */
  private $altura;

  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   *
  */
  private $largura;

  /**
   * @ORM\Column(type="float")
   *
  */
  private $metragem;

  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   *
  */
  private $qtde;

  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   *
  */
  private $preco;


  /**
   * @ORM\Column(type="float")
   * @Assert\NotBlank()
   *
  */
  private $pr_venda;


  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of preco
   */ 
  public function getPreco()
  {
    return $this->preco;
  }

  /**
   * Set the value of preco
   *
   * @return  self
   */ 
  public function setPreco($preco)
  {
    $this->preco = $preco;

    return $this;
  }

  /**
   * Get the value of qtde
   */ 
  public function getQtde()
  {
    return $this->qtde;
  }

  /**
   * Set the value of qtde
   *
   * @return  self
   */ 
  public function setQtde($qtde)
  {
    $this->qtde = $qtde;

    return $this;
  }

  /**
   * Get the value of metragem
   */ 
  public function getMetragem()
  {
    return $this->metragem;
  }

  /**
   * Set the value of metragem
   *
   * @return  self
   */ 
  public function setMetragem($metragem)
  {
    $this->metragem = $metragem;

    return $this;
  }

  /**
   * Get the value of largura
   */ 
  public function getLargura()
  {
    return $this->largura;
  }

  /**
   * Set the value of largura
   *
   * @return  self
   */ 
  public function setLargura($largura)
  {
    $this->largura = $largura;

    return $this;
  }

  /**
   * Get the value of altura
   */ 
  public function getAltura()
  {
    return $this->altura;
  }

  /**
   * Set the value of altura
   *
   * @return  self
   */ 
  public function setAltura($altura)
  {
    $this->altura = $altura;

    return $this;
  }

  /**
   * Get the value of cod_prod
   */ 
  public function getCodProd()
  {
    return $this->cod_prod;
  }

  /**
   * Set the value of cod_prod
   *
   * @return  self
   */ 
  public function setCodProd($cod_prod)
  {
    $this->cod_prod = $cod_prod;

    return $this;
  }

  /**
   * Get the value of cod_vend
   */ 
  public function getCodVend()
  {
    return $this->cod_vend;
  }

  /**
   * Set the value of cod_vend
   *
   * @return  self
   */ 
  public function setCodVend($cod_vend)
  {
    $this->cod_vend = $cod_vend;

    return $this;
  }

  /**
   * Get the value of nome
   */ 
  public function getNome()
  {
    return $this->nome;
  }

  /**
   * Set the value of nome
   *
   * @return  self
   */ 
  public function setNome($nome)
  {
    $this->nome = $nome;

    return $this;
  }

  /**
   * Get the value of pr_venda
   */ 
  public function getPr_venda()
  {
    return $this->pr_venda;
  }

  /**
   * Set the value of pr_venda
   *
   * @return  self
   */ 
  public function setPr_venda($pr_venda)
  {
    $this->pr_venda = $pr_venda;

    return $this;
  }
}