<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity
 * @ORM\Table(name="usuarios")
 * @UniqueEntity(
 *     fields={"nome"},
 *     message="Username já cadastrado"
 * )
 */
class Users {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;

  /**
   * @ORM\Column(type="string", unique=true)
   * @Assert\NotBlank()
   *
   */
  private $nome;

  /**
   * @ORM\Column(type="text")
   */
  private $senha;

   /**
   * @ORM\Column(type="text", length=1)
   */
  private $nivel;


   /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   */
  private $cod_fun;


  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of cod_fun
   */ 
  public function getCodFun()
  {
    return $this->cod_fun;
  }

  /**
   * Set the value of cod_fun
   *
   * @return  self
   */ 
  public function setCodFun($cod_fun)
  {
    $this->cod_fun = $cod_fun;

    return $this;
  }

  /**
   * Get the value of nivel
   */ 
  public function getNivel()
  {
    return $this->nivel;
  }

  /**
   * Set the value of nivel
   *
   * @return  self
   */ 
  public function setNivel($nivel)
  {
    $this->nivel = $nivel;

    return $this;
  }

  /**
   * Get the value of senha
   */ 
  public function getSenha()
  {
    return $this->senha;
  }

  /**
   * Set the value of senha
   *
   * @return  self
   */ 
  public function setSenha($senha)
  {
    $this->senha = $senha;

    return $this;
  }

  /**
   * Get the value of nome
   */ 
  public function getNome()
  {
    return $this->nome;
  }

  /**
   * Set the value of nome
   *
   * @return  self
   */ 
  public function setNome($nome)
  {
    $this->nome = $nome;

    return $this;
  }
}