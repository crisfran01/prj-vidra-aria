<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="condpg")
 */
class ContasReceb {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;

    /**
   * @ORM\Column(type="integer")
   */
  private $cod_vend;

  /**
   * @ORM\Column(type="text")
   * @Assert\NotBlank()
   */
  private $status;

  /**
   * @ORM\Column(type="text")
   * @Assert\NotBlank()
   */
  private $data_criacao;


  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of cod_vend
   */ 
  public function getCod_vend()
  {
    return $this->cod_vend;
  }

  /**
   * Set the value of cod_vend
   *
   * @return  self
   */ 
  public function setCod_vend($cod_vend)
  {
    $this->cod_vend = $cod_vend;

    return $this;
  }

  /**
   * Get the value of status
   */ 
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set the value of status
   *
   * @return  self
   */ 
  public function setStatus($status)
  {
    $this->status = $status;

    return $this;
  }

  /**
   * Get the value of data_criacao
   */ 
  public function getDataCriacao()
  {
    return $this->data_criacao;
  }

  /**
   * Set the value of data_criacao
   *
   * @return  self
   */ 
  public function setDataCriacao($data_criacao)
  {
    $this->data_criacao = $data_criacao;

    return $this;
  }
}