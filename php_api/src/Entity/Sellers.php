<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity
 * @ORM\Table(name="fornecedores")
 * @UniqueEntity(
 *     fields={"cnpj"},
 *     message="CNPJ já cadastrado"
 * )
 */
class Sellers {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank()
   *
   */
  private $razao_social;

  /**
   * @ORM\Column(type="text")
   */
  private $nome_fantasia;

   /**
   * @ORM\Column(type="text")
   */
  private $servico;


   /**
   * @ORM\Column(type="text", length=255, unique=true)
   * @Assert\NotBlank()
   */
  private $cnpj;


   /**
   * @ORM\Column(type="text")
   */
  private $ie;


   /**
   * @ORM\Column(type="text")
   */
  private $end;

    /**
   * @ORM\Column(type="integer")
   */
  private $num;

    /**
   * @ORM\Column(type="text")
   */
  private $bairro;

    /**
   * @ORM\Column(type="text")
   */
  private $cidade;

    /**
   * @ORM\Column(type="text")
   */
  private $uf;

    /**
   * @ORM\Column(type="text")
   */
  private $cep;

    /**
   * @ORM\Column(type="text")
   */
  private $whats;

  /**
   * @ORM\Column(type="text")
   */
  private $tel;

  /**
   * @ORM\Column(type="text")
   */
  private $email;

  /**
   * @ORM\Column(type="text")
   */
  private $obs;

  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of end
   */ 
  public function getEnd()
  {
    return $this->end;
  }

  /**
   * Set the value of end
   *
   * @return  self
   */ 
  public function setEnd($end)
  {
    $this->end = $end;

    return $this;
  }

  /**
   * Get the value of num
   */ 
  public function getNum()
  {
    return $this->num;
  }

  /**
   * Set the value of num
   *
   * @return  self
   */ 
  public function setNum($num)
  {
    $this->num = $num;

    return $this;
  }

  /**
   * Get the value of bairro
   */ 
  public function getBairro()
  {
    return $this->bairro;
  }

  /**
   * Set the value of bairro
   *
   * @return  self
   */ 
  public function setBairro($bairro)
  {
    $this->bairro = $bairro;

    return $this;
  }

  /**
   * Get the value of cidade
   */ 
  public function getCidade()
  {
    return $this->cidade;
  }

  /**
   * Set the value of cidade
   *
   * @return  self
   */ 
  public function setCidade($cidade)
  {
    $this->cidade = $cidade;

    return $this;
  }

  /**
   * Get the value of whats
   */ 
  public function getWhats()
  {
    return $this->whats;
  }

  /**
   * Set the value of whats
   *
   * @return  self
   */ 
  public function setWhats($whats)
  {
    $this->whats = $whats;

    return $this;
  }

  /**
   * Get the value of tel
   */ 
  public function getTel()
  {
    return $this->tel;
  }

  /**
   * Set the value of tel
   *
   * @return  self
   */ 
  public function setTel($tel)
  {
    $this->tel = $tel;

    return $this;
  }

  /**
   * Get the value of email
   */ 
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set the value of email
   *
   * @return  self
   */ 
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
   * Get the value of obs
   */ 
  public function getObs()
  {
    return $this->obs;
  }

  /**
   * Set the value of obs
   *
   * @return  self
   */ 
  public function setObs($obs)
  {
    $this->obs = $obs;

    return $this;
  }

  /**
   * Get the value of uf
   */ 
  public function getUf()
  {
    return $this->uf;
  }

  /**
   * Set the value of uf
   *
   * @return  self
   */ 
  public function setUf($uf)
  {
    $this->uf = $uf;

    return $this;
  }

  /**
   * Get the value of cep
   */ 
  public function getCep()
  {
    return $this->cep;
  }

  /**
   * Set the value of cep
   *
   * @return  self
   */ 
  public function setCep($cep)
  {
    $this->cep = $cep;

    return $this;
  }

  /**
   * Get the value of nome_fantasia
   */ 
  public function getNomeFantasia()
  {
    return $this->nome_fantasia;
  }

  /**
   * Set the value of nome_fantasia
   *
   * @return  self
   */ 
  public function setNomeFantasia($nome_fantasia)
  {
    $this->nome_fantasia = $nome_fantasia;

    return $this;
  }

  /**
   * Get the value of razao_social
   */ 
  public function getRazaoSocial()
  {
    return $this->razao_social;
  }

  /**
   * Set the value of razao_social
   *
   * @return  self
   */ 
  public function setRazaoSocial($razao_social)
  {
    $this->razao_social = $razao_social;

    return $this;
  }

  /**
   * Get the value of servico
   */ 
  public function getServico()
  {
    return $this->servico;
  }

  /**
   * Set the value of servico
   *
   * @return  self
   */ 
  public function setServico($servico)
  {
    $this->servico = $servico;

    return $this;
  }

  /**
   * Get the value of cnpj
   */ 
  public function getCnpj()
  {
    return $this->cnpj;
  }

  /**
   * Set the value of cnpj
   *
   * @return  self
   */ 
  public function setCnpj($cnpj)
  {
    $this->cnpj = $cnpj;

    return $this;
  }

  /**
   * Get the value of ie
   */ 
  public function getIe()
  {
    return $this->ie;
  }

  /**
   * Set the value of ie
   *
   * @return  self
   */ 
  public function setIe($ie)
  {
    $this->ie = $ie;

    return $this;
  }
}
  