<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="condpg")
 */
class CondPgs {

  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $cod;

  /**
   * @ORM\Column(type="text")
   * @Assert\NotBlank()
   */
  private $descricao;

  /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   */
  private $parcelas;


  /**
   * @ORM\Column(type="integer")
   * @Assert\NotBlank()
   */
  private $data_first;

  /**
   * Get the value of cod
   */ 
  public function getCod()
  {
    return $this->cod;
  }

  /**
   * Set the value of cod
   *
   * @return  self
   */ 
  public function setCod($cod)
  {
    $this->cod = $cod;

    return $this;
  }

  /**
   * Get the value of descricao
   */ 
  public function getDescricao()
  {
    return $this->descricao;
  }

  /**
   * Set the value of descricao
   *
   * @return  self
   */ 
  public function setDescricao($descricao)
  {
    $this->descricao = $descricao;

    return $this;
  }

  /**
   * Get the value of parcelas
   */ 
  public function getParcelas()
  {
    return $this->parcelas;
  }

  /**
   * Set the value of parcelas
   *
   * @return  self
   */ 
  public function setParcelas($parcelas)
  {
    $this->parcelas = $parcelas;

    return $this;
  }

  /**
   * Get the value of data_first
   */ 
  public function getDataFirst()
  {
    return $this->data_first;
  }

  /**
   * Set the value of data_first
   *
   * @return  self
   */ 
  public function setDataFirst($data_first)
  {
    $this->data_first = $data_first;

    return $this;
  }
}