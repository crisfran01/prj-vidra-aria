CREATE VIEW full_item AS
SELECT i.cod, p.nome, i.cod_vend, i.cod_prod, i.altura, i.largura, i.metragem,i.qtde, i.preco, p.pr_venda
FROM items i inner join produtos p on p.cod = i.cod_prod


CREATE VIEW full_sale AS
SELECT 
v.cod ,
v.obs ,
v.data ,
c.nome ,
v.cod_fun ,
v.cod_cli ,
v.cod_pg ,
v.valor ,
p.descricao as name_pg ,
f.nome as name_fun 
FROM vendas v 
inner join  condpg p on p.cod = v.cod_pg
inner join funcionarios f on p.cod = v.cod_fun
inner join clientes c on p.cod = v.cod_cli


CREATE VIEW full_user AS
SELECT 
u.cod ,
u.nome as username,
f.nome ,
u.senha ,
u.nivel,
u.cod_fun
FROM usuarios u
inner join funcionarios f on f.cod = u.cod_fun